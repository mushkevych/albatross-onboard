/*
	Daemon to listen to all communication of UDP port and write it to log files

	date: September, 2010
	author: Bohdan Mushkevych
 */

void display_usage(void);
FILE *gfopen(char *filename, char *mode);
void datagram_callback(int cmd, void *user_data);
long long timeofday_milliseconds();

#define DEFAULT_LOGGING_RATE 1
