'''
Unit test to verify Route and Waypoint essentials

Created on May 3, 2010
@author: Bohdan Mushkevych
'''
import unittest
from waypoint import Waypoint
from route import Route

class TestRoute(unittest.TestCase):
		'''
		Unit tests covers creation and comparison or Waypoint and Route 
		'''

		def test_waypoint_equity(self):
				''' creates and compares Waypoints'''
				wp_1      = Waypoint (_id=1, latitude=33.95, longtitude=118.4, altitude=1000, lwc=0)
				wp_1_copy = Waypoint (_id=1, latitude=33.95, longtitude=118.4, altitude=1000, lwc=0)
				wp_2 = Waypoint (_id=2, latitude=35.20, longtitude=110.0, altitude=1100, lwc=0)
				wp_3 = Waypoint ()
				
				self.assertEqual(wp_1, wp_1)
				self.assertEqual(wp_1, wp_1_copy)
				self.assertNotEqual(wp_1, wp_2)
				self.assertNotEqual(wp_1, wp_3)
				self.assertNotEqual(wp_1, None)

		
		def test_route_equity(self):
				''' creates and compares Routes '''
				wp_0 = Waypoint (_id=0, latitude=33.95, longtitude=118.4, altitude=900, lwc='zero')
				wp_1 = Waypoint (_id=1, latitude=33.95, longtitude=118.4, altitude=1000, lwc='another command')
				wp_2 = Waypoint (_id=2, latitude=35.20, longtitude=110.0, altitude=1100, lwc=0)
				wp_3 = Waypoint (_id=3, latitude=37.30, longtitude=105.5, altitude=1200, lwc=0)
				wp_4 = Waypoint (_id=4, latitude=39.40, longtitude=100.0, altitude=1300, lwc=0)
				wp_5 = Waypoint (_id=5, latitude=41.50, longtitude=95.0, altitude=800, lwc=0)
				wp_6 = Waypoint (_id=6, latitude=43.60, longtitude=90.5, altitude=700, lwc=0)
				wp_7 = Waypoint (_id=7, latitude=42.50, longtitude=85.0, altitude=600, lwc=0)
				wp_8 = Waypoint (_id=8, latitude=41.40, longtitude=80.5, altitude=500, lwc=0)
				wp_9 = Waypoint (_id=9, latitude=40.633333, longtitude=73.783333, altitude=800, lwc=0)

				wp_8_copy = Waypoint (_id=8, latitude=41.40, longtitude=80.5, altitude=500, lwc=0)
				wp_9_copy = Waypoint (_id=9, latitude=40.633333, longtitude=73.783333, altitude=800, lwc=0)
				wp_10 = Waypoint (_id=8, latitude=39.40, longtitude=70.5, altitude=750, lwc='command')

				route_1 = Route()
				route_1.add(wp_1)
				route_1.add(wp_2)
				route_1.add(wp_3)
				route_1.add(wp_4)
				route_1.add(wp_5)
				route_1.add(wp_6)
				route_1.add(wp_7)
				route_1.add(wp_8)
				route_1.add(wp_9)
				
				route_2 = Route()
				route_2.add(wp_1)
				route_2.add(wp_2)
				route_2.add(wp_3)
				route_2.add(wp_4)
				route_2.add(wp_5)
				route_2.add(wp_6)
				route_2.add(wp_7)
				route_2.add(wp_8_copy)
				route_2.add(wp_9_copy)

				route_3 = Route()
				route_3.add(wp_0)
				route_3.add(wp_2)
				route_3.add(wp_3)
				route_3.add(wp_4)
				route_3.add(wp_5)
				route_3.add(wp_6)
				route_3.add(wp_7)
				route_3.add(wp_8)
				route_3.add(wp_10)
				
				route_4 = Route()
				route_4.add(wp_0)
				route_4.add(wp_2)
				route_4.add(wp_3)
				route_4.add(wp_4)
				route_4.add(wp_5)
				route_4.add(wp_6)
				route_4.add(wp_7)
				route_4.add(wp_8)

				self.assertEqual(route_1, route_2)
				self.assertNotEqual(route_1, route_3)
				self.assertNotEqual(route_1, None)
				self.assertNotEqual(route_3, route_4)

    
if __name__ == "__main__":
		#import sys;sys.argv = ['', 'Test.testName']
		
		unittest.main()

