#define RADIO_REPLY_TIMEOUT 1 //Number of seconds to wait for response from radio
#define RADIO_BAUDRATE B57600

//#define ENTER_AT_COMMAND_MODE {'A','T','+','+','+',0x0D}
#define ENTER_AT_COMMAND_MODE {0x41,0x54,0x2b,0x2b,0x2b,0x0D}
//#define ENTER_AT_COMMAND_MODE_RESPONSE {0xCC,'C','O','M'}
#define ENTER_AT_COMMAND_MODE_RESPONSE {0xCC,0x43,0x4f,0x4d}
#define ENTER_AT_CONFIG_MODE {0xCC,0x65}
#define ENTER_AT_CONFIG_MODE_RESPONSE {0x65}
#define STATUS_REQUEST {0xCC,0x00,0x00}
#define STATUS_REQUEST_RESPONSE {0xCC,'X','Y'} //X=Firmware version number, Y=Operation mode
#define EXIT_AT_COMMAND_MODE {0xCC,'A','T','O',0x0D}
#define EXIT_AT_COMMAND_MODE_RESPONSE {0xCC,'D','A','T'}
#define REPORT_LAST_VALID_RSSI {0xCC,0x22}
#define REPORT_LAST_VALID_RSSI_RESPONSE {0xCC,'X'} //X=RSSI
#define READ_TEMPERATURE {0xCC,0xA4}
#define READ_TEMPERATURE_RESPONSE {0xCC,'X'} //X=Temperature
#define ENTER_SLEEP {0xCC,0x06}
#define ENTER_SLEEP_RESPONSE {0xCC,'X'} //X=Channel Number
#define EXIT_SLEEP {0xCC,0x07}
#define EXIT_SLEEP_RESPONSE {0xCC,'X'} //X=Channel Number
#define SET_MODE {0xCC,0x03,'X'}	//X=mode, 2=client, 3=server
#define SET_MODE_RESPONSE {0xCC,'Y','X'}	//Y=FW version, X=mode
#define SET_DEST_ADDRESS {0xCC,0x10,'X','Y','Z'}	//X=byte4 of dest mac addy, Y=byte5, Z=byte6
#define SET_DEST_ADDRESS_RESPONSE {0xCC,'X','Y','Z'}	//X=byte4 of dest mac addy, Y=byte5, Z=byte6
#define GET_DEST_ADDRESS {0xCC,0x11}	//X=byte4 of dest mac addy, Y=byte5, Z=byte6
#define GET_DEST_ADDRESS_RESPONSE {0xCC,'X','Y','Z'}	//X=byte4 of dest mac addy, Y=byte5, Z=byte6
#define SET_RADIO_POWER {0xCC,0x25,'X'}	//X=max power
#define SET_RADIO_POWER_RESPONSE {0xCC,'X'}	//X=max power
#define SET_AUTO_DEST_ADDRESS {0xCC,0x15,'X'}
#define SET_AUTO_DEST_ADDRESS_RESPONSE {0xCC,0x1}

#define DEBUG_RADIO_RAW 1
#define DEBUG_RADIO 1
#define DEBUG_RADIO_MSG(X) if(DEBUG_RADIO) {printf("Radio Debug Message: %s\n",X);}

#define RADIO_CONFIG_NUM_LINES 3
#define RADIO_CONFIG_MAX_LINE_LENGTH 100
#define RADIO_CONFIG_LINE_MODE 0
#define RADIO_CONFIG_LINE0_FORMAT_STRING "MODE:%C"
#define RADIO_CONFIG_LINE_DEST 1
#define RADIO_CONFIG_LINE1_FORMAT_STRING "DEST:0x%X,0x%X,0x%X"
#define RADIO_CONFIG_LINE_POWER 2
#define RADIO_CONFIG_LINE2_FORMAT_STRING "POWER:0x%X"

int radio_enter_AT_mode(FILE *serial);
int radio_exit_AT_mode(FILE *serial);
int radio_get_status(FILE *serial, uint8_t *firmwareVersion, uint8_t *status);
int8_t radio_get_last_rssi(FILE *serial, uint8_t *rssi);
int8_t radio_read_temperature(FILE *serial, uint8_t *temp);
uint8_t radio_send_raw_command(FILE *serial, uint8_t *data, uint8_t len);
uint8_t radio_receive_raw_command(FILE *serial, uint8_t *data, uint8_t len);
int radio_enter_sleep(FILE *serial, uint8_t *channel);
int radio_exit_sleep(FILE *serial, uint8_t *channel);
uint8_t radio_configure_from_file(FILE *serial, char *filename);
