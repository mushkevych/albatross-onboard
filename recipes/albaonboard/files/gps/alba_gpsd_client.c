/* $Id: cgps.c 5496 2009-03-18 16:43:52Z jfrancis $ */
/*
* Copyright (c) 2005 Jeff Francis <jeff@gritch.org>
*
* Permission to use, copy, modify, and distribute this software for any
* purpose with or without fee is hereby granted, provided that the above
* copyright notice and this permission notice appear in all copies.
*
* THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
* WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
* ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
* WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
* ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
* OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

/*
Jeff Francis
jeff@gritch.org
*/

#define UNUSED

#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <errno.h>
#include <assert.h>
#include <signal.h>

#include <gps.h>

#include "../protocol/datagram.h"
#include "../protocol/protocol.h"

static struct gps_data_t *gpsdata;
static time_t status_timer;    /* Time of last state change. */
static time_t misc_timer;    /* Misc use timer. */

// gpsd units of measurements
static float altfactor = 1;
static float speedfactor = MPS_TO_KPH;
static char *altunits = "m";
static char *speedunits = "kph";

static int got_gps_type=0;
static char gps_type[26];

//Datagram for sending
static datagrizzle_t dgSend;

/* Function to call when we're all done.  Does a bit of clean-up. */
static void die(int sig UNUSED)
{
	/* Ignore signals. */
	(void)signal(SIGINT,SIG_IGN);
	(void)signal(SIGHUP,SIG_IGN);

	/* We're done talking to gpsd. */
	(void)gps_close(gpsdata);

	/* Bye! */
	exit(0);
}

/* function sends datagram to UDP point */
static void send_datagram(void)
{
	datagram_set_destination(&dgSend, GROUNDSTATION_D_BITMASK | AUTOPILOT_D_BITMASK | COMM_D_BITMASK);
	datagram_set_command(&dgSend, LWC_DATA);
	datagram_set_sender(&dgSend, STATE_D_BITMASK);
	datagram_pack(&dgSend);
	datagram_send(&dgSend);
}


/* This gets called once for each new sentence until we figure out
what's going on and switch to either update_gps_panel() or
update_compass_panel(). */
static void update_probe(struct gps_data_t *gpsdata,
						 char *message,
						 size_t len)
{
	/* Send an 'i' once per second until we figure out what the GPS
	device is. */
	if(time(NULL)-misc_timer > 2) {
		(void)gps_query(gpsdata, "i\n");
		(void)fprintf(stderr,"Probing...\n");
		misc_timer=time(NULL);
	}

	assert(message != NULL);
	if(strncmp(message,"GPSD,I=",6) == 0) {
		message+=7;
		(void)strlcpy(gps_type, message, sizeof(gps_type));
		got_gps_type=1;
	}
}


/* This gets called once for each new GPS sentence. */
static void prepare_datagram(struct gps_data_t *gpsdata,
						 char *message,
						 size_t len)
{
	int newstate;

	/* Fill in the latitude. */
	if (gpsdata->fix.mode >= MODE_2D && isnan(gpsdata->fix.latitude)==0) {
		datagram_add_float(&dgSend, STATE_D_LATITUDE, gpsdata->fix.latitude);
		
//		(void)printf("%s %c \n", 
//			deg_to_str(deg_type,  fabs(gpsdata->fix.latitude)), 
//			(gpsdata->fix.latitude < 0) ? 'S' : 'N');
	} 

	/* Fill in the longitude. */
	if (gpsdata->fix.mode >= MODE_2D && isnan(gpsdata->fix.longitude)==0) {
		datagram_add_float(&dgSend, STATE_D_LONGITUDE, gpsdata->fix.longitude);
	
//		(void)printf("%s %c", 
//			deg_to_str(deg_type,  fabs(gpsdata->fix.longitude)), 
//			(gpsdata->fix.longitude < 0) ? 'W' : 'E');
	} 

	/* Fill in the altitude. */
	if (gpsdata->fix.mode == MODE_3D && isnan(gpsdata->fix.altitude)==0) {
		datagram_add_float(&dgSend, STATE_D_ALTITUDE, gpsdata->fix.altitude);

		(void)printf("%.1f %s", gpsdata->fix.altitude * altfactor, altunits);
	}

	/* Fill in the speed. */
	if (gpsdata->fix.mode >= MODE_2D && isnan(gpsdata->fix.track)==0) {
		datagram_add_float(&dgSend, STATE_D_SPEED_GLOBAL_X, gpsdata->fix.speed);
		datagram_add_float(&dgSend, STATE_D_SPEED_GROUND, gpsdata->fix.speed);

		(void)printf("%.1f %s", gpsdata->fix.speed * speedfactor, speedunits);
	}

	datagram_add_byte(&dgSend,  STATE_D_SATS_IN_VIEW, gpsdata->satellites_visible);

	/* Fill in the heading. */
	if (gpsdata->fix.mode >= MODE_2D && isnan(gpsdata->fix.track)==0) {
		datagram_add_float(&dgSend, STATE_D_HEADING, gpsdata->fix.track);

		(void)printf("%.1f deg (true)", gpsdata->fix.mode);
	}

	/* Fill in the GPS status and the time since the last state change. */
	if (gpsdata->online == 0) {
		newstate = 0;
		(void)printf("OFFLINE");
	} else {
		newstate = gpsdata->fix.mode;
		datagram_add_byte(&dgSend, STATE_D_GPS_MODE, gpsdata->fix.mode);

		switch (gpsdata->fix.mode) {
			case MODE_2D:
				(void)printf("2D %sFIX (%d secs)",(gpsdata->status==STATUS_DGPS_FIX)?"DIFF ":"", (int) (time(NULL) - status_timer));
				break;
			case MODE_3D:
				(void)printf("3D %sFIX (%d secs)",(gpsdata->status==STATUS_DGPS_FIX)?"DIFF ":"", (int) (time(NULL) - status_timer));
				break;
			default:
				(void)printf("NO FIX (%d secs)", (int) (time(NULL) - status_timer));
				break;
		}
	}

	/* Fill in the rate of climb. */
	if (gpsdata->fix.mode == MODE_3D && isnan(gpsdata->fix.climb)==0) {
		datagram_add_float(&dgSend, STATE_D_SPEED_GLOBAL_Z, gpsdata->rtcm2.ecef.x);

		(void)printf("%.1f %s/min",
		gpsdata->fix.climb * altfactor * 60, altunits);
	}

	send_datagram();
}


int main(int argc, char *argv[])
{
	char *err_str = NULL;
	struct timeval timeout;
	fd_set rfds;
	int data;
	
	//Initialize Sender Datagram
	datagram_clear(&dgSend);	
	datagram_init(&dgSend, DATAGRAM_MAX_PAYLOAD_SIZE);

	char* server = argv[1];
	char* port = argv[2];
	char* device = argv[3];
	(void)strlcpy(gps_type, "unknown", sizeof(gps_type));
		
	/* Open the stream to gpsd. */
	/*@i@*/gpsdata = gps_open(server, port);
	if (!gpsdata) {
		switch ( errno ) {
			case NL_NOSERVICE:  err_str = "can't get service entry"; break;
			case NL_NOHOST:     err_str = "can't get host entry"; break;
			case NL_NOPROTO:    err_str = "can't get protocol entry"; break;
			case NL_NOSOCK:     err_str = "can't create socket"; break;
			case NL_NOSOCKOPT:  err_str = "error SETSOCKOPT SO_REUSEADDR"; break;
			case NL_NOCONNECT:  err_str = "can't connect to host"; break;
			default:		err_str = "Unknown"; break;
		}
		(void)fprintf( stderr,
			"alba_gpsd_client: no gpsd running or network error: %d, %s\n",
			errno, err_str);
		exit(2);
	}

	/* Set both timers to now. */
	status_timer = time(NULL);
	misc_timer = status_timer;
	
	/* Here's where updates go now that things are established. */
	gps_set_raw_hook(gpsdata, prepare_datagram);

	(void)gps_stream(gpsdata, WATCH_ENABLE|WATCH_NEWSTYLE, NULL);

	/* heart of the client */
	for (;;) {

		/* watch to see when it has input */
		FD_ZERO(&rfds);
		FD_SET(gpsdata->gps_fd, &rfds);

		/* wait up to five seconds. */
		timeout.tv_sec = 1;
		timeout.tv_usec = 0;

		/* check if we have new information */
		data = select(gpsdata->gps_fd + 1, &rfds, NULL, NULL, &timeout);

		if (data == -1) {
			fprintf( stderr, "alba_gpsd_client: socket error 3\n");
			exit(2);
		}
		else if( data ) {
			/* code that calls gps_poll(gpsdata) */
			if (gps_poll(gpsdata) != 0) {
			    fprintf( stderr, "alba_gpsd_client: socket error 4\n");
			    die(1);
			}
		}

	}

}
