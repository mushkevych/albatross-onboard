"""Pyrex wrapper for parsegram.h functions from onboard/lib
Provides the Parsegram class that encapsulates the C API.

Example: SETING & GETTING
  import parsegram
  d = parsegram.Parsegram()
  key = 0x1
  value = 123.456
  d.put_float(key, value)
  d.get_float(key)

Bohdan Mushkevych, 2009."""

#------------------------------------------------------------------------------
# Includes
include "common_constants.pyx"

#import datetime
#from datetime import datetime

#------------------------------------------------------------------------------
# A generic parsegram exception
class IpcException(Exception):
  pass

#------------------------------------------------------------------------------
# The class Parsegram (Pythonized API)
cdef class Parsegram:

  def __cinit__ (self):
    """Allocate and initialize the C structure (parsegram_t) and
    run parsegram_init on it."""
    
    # Allocate/init the C structure
    self.pg = <parsegram_t*>malloc(sizeof(parsegram_t))
    if self.pg == NULL:
      raise MemoryError
    parsegram_init(self.pg)

  def __init__ (self):
    """ allocate Python objects"""
    self.dati = datetime.now()  # the Python datetime structure

  def __dealloc__(self):
    parsegram_clear(self.pg)
    parsegram_delete(self.pg)
    free(self.pg)

  # **************************** Parsegram Init functions *****************************
  def clear(self):
    parsegram_clear(self.pg)
    
  # **************************** Parsegram Access functions *****************************
  def put(self, key, value):
    parsegram_put(self.pg, key, <unsigned char *> value)
    
  def remove(self, key):
    parsegram_remove(self.pg, key)

  def get(self, key):
    cdef uint8_t* buffer
    buffer = parsegram_get(self.pg, key)
    if buffer == NULL:
      return None
    temp = PyString_FromStringAndSize(<char*>buffer, MAX_MESSAGE_LENGTH)
	
    return temp

  # **************************** Parsegram Conversions functions *****************************
  def put_float(self, key, value):
    parsegram_put_float(self.pg, key, value)
    
  def get_float(self, key):
    return parsegram_get_as_float(self.pg, key)

  def put_byte(self, key, value):
    parsegram_put_byte(self.pg, key, value)
  
  def get_byte(self, key):
    return parsegram_get_as_byte(self.pg, key)
  
  def put_short(self, key, value):
    parsegram_put_short(self.pg, key, value)
  
  def get_short(self, key):
    return parsegram_get_as_short(self.pg, key)

  def put_int(self, key, value):
    parsegram_put_integer(self.pg, key, value)
  
  def get_int(self, key):
    return parsegram_get_as_integer(self.pg, key)

  def get_size(self):
    return parsegram_get_size(self.pg)

  def get_keys(self):
    return <char *>parsegram_get_keys(self.pg)

  def get_datetime(self):
    return self.dati

  def reset_datetime(self):
    dati = datetime.now()
