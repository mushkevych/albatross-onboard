#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>

#ifdef BIG_ENDIAN
	#include <netinet/in.h>
#else
	#define ntohs(x)
	#define ntohl(x)
	#define htonl(x)
	#define htons(x)
#endif

#include "albatross_std.h"
#include "datagram.h"
#include "udp_multicast.h"
#include "protocol.h"

// **************************** Datagram Header setters and getters *****************************
//Datagram Header setters and getters to overcome endianess issues.
//The data is sent in network byte order (big endian). Because arm
//and PPC are also bigendian ntohx() and htonx() are no-ops so processing is
//fastest on these systems.
uint16_t datagram_get_destination(datagrizzle_t *d)
{
	uint16_t ret = 0;
	ret |= (0xFF00 & (d->payload.header[DATAGRAM_DESTHI] << 8));
	ret |= d->payload.header[DATAGRAM_DESTLO];
	return ret;
}
void datagram_set_destination(datagrizzle_t *d, uint16_t dest)
{
	d->payload.header[DATAGRAM_DESTHI] = HI_BYTE(dest);
	d->payload.header[DATAGRAM_DESTLO] = LO_BYTE(dest);
}

uint16_t datagram_get_sender(datagrizzle_t *d)
{
	uint16_t ret = 0;
	ret |= (0xFF00 & (d->payload.header[DATAGRAM_SENDERHI] << 8));
	ret |= d->payload.header[DATAGRAM_SENDERLO];
	return ret;
}
void datagram_set_sender(datagrizzle_t *d, uint16_t send)
{
	d->payload.header[DATAGRAM_SENDERHI] = HI_BYTE(send);
	d->payload.header[DATAGRAM_SENDERLO] = LO_BYTE(send);
}

// command being transferred
uint16_t datagram_get_command(datagrizzle_t *d)
{
	uint16_t ret = 0;
	ret |= (0xFF00 & (d->payload.header[DATAGRAM_CMDHI] << 8));
	ret |= d->payload.header[DATAGRAM_CMDLO];
	return ret;
}
void datagram_set_command(datagrizzle_t *d, uint16_t cmd)
{
	d->payload.header[DATAGRAM_CMDHI] = HI_BYTE(cmd);
	d->payload.header[DATAGRAM_CMDLO] = LO_BYTE(cmd);
}

// check sum for the datagram payload
uint16_t datagram_get_chsum(datagrizzle_t *d)
{
	uint16_t ret = 0;
	ret |= (0xFF00 & (d->payload.header[DATAGRAM_DATA_CHSUMHI] << 8));
	ret |= d->payload.header[DATAGRAM_DATA_CHSUMLO];
	return ret;
}
void datagram_set_chsum(datagrizzle_t *d, uint16_t ch)
{
	d->payload.header[DATAGRAM_DATA_CHSUMHI] = HI_BYTE(ch);
	d->payload.header[DATAGRAM_DATA_CHSUMLO] = LO_BYTE(ch);
}

// check sum for the datagram's header
uint8_t datagram_get_hchsum(datagrizzle_t *d)
{
	return d->payload.header[DATAGRAM_HEADER_CHSUM];
}
void datagram_set_hchsum(datagrizzle_t *d, uint8_t hch)
{
	d->payload.header[DATAGRAM_HEADER_CHSUM] = hch;
}

// actual payload size, transferred by datagram
uint8_t datagram_get_payload_size(datagrizzle_t *d)
{
	return d->payload.header[DATAGRAM_PAYLOAD_SIZE];
}
void datagram_set_payload_size(datagrizzle_t *d, uint8_t size)
{
	d->payload.header[DATAGRAM_PAYLOAD_SIZE] = size;
}

// datagram bitmask is used to comparing the dest field and the bitMask
// and define whether datagrams is for us
// @see datagram_process
void datagram_set_receive_bitmask(datagrizzle_t	*d, uint16_t bitMask)
{
	d->receiveBitmask = bitMask;
}

// registers callback functions for given datagram
void datagram_set_callback(datagrizzle_t *d,void(*fptr)(int, void*), void *user_data)
{
	d->received_callback = fptr;
	d->received_callback_user_data = user_data;
	//printf("Callback address = 0x%08X\n", d->received_callback);
}

// set udp multicast port
void datagram_set_port(datagrizzle_t *d, int port)
{
	d->port = port;
}


// **************************** Datagram Header checksum functions *****************************

//Checks the header for validity, including checking
//the Sync bytes, and the HCHSUM checksum for validity
uint8_t datagram_check_header(datagrizzle_t *d)
{
	uint8_t j = 0;
	bool_t crcCheck = FALSE;
	bool_t headerCheck = FALSE;

	//Check Sync Bytes
	if((d->payload.header[DATAGRAM_SYNC0] == DATAGRAM_SYNC0_BYTE) && (d->payload.header[DATAGRAM_SYNC1] == DATAGRAM_SYNC1_BYTE))
		headerCheck = TRUE;
	else
		headerCheck = FALSE;

	//8-bit XOR checksum
	j = datagram_build_header_checksum(d);

	if(j == d->payload.header[DATAGRAM_HEADER_CHSUM])
		crcCheck = TRUE;
	else
		crcCheck = FALSE;

	//Both have to be TRUE for a valid header
	return (headerCheck && crcCheck ? TRUE : FALSE);
}

// Builds a header checksum for a datagram.
// Should be called prior to sending
// Returns the built checksum
uint8_t datagram_build_header_checksum(datagrizzle_t *d)
{
	uint8_t crc, i;
	uint8_t sizeOfHeader = DATAGRAM_HEADER_LEN-1;
	crc = 0;

	//XOR Checksum
	for(i = 0; i < sizeOfHeader; i++)
		crc ^= d->payload.header[i];

	return crc;
}

// **************************** Datagram Payload checksum functions *****************************

//Performs a fletcher check on the datagram payload, from the
//start address of buffer upto size number of bytes.
//Returns TRUE for succes
uint8_t datagram_check_fletcher(datagrizzle_t *d)
{
	uint16_t j,k = 0;

	//Calculated
	j = datagram_build_fletcher_checksum(d);

	//Expected - Note its sent in beg endial so use setters and getters
	k = datagram_get_chsum(d);

	//Compare and return
	return (j == k ? TRUE : FALSE);
}

//Builds a datagram payload fletcher checksum. Should be used to populate the header
//struct prior to sending
//Returns the built checksum
uint16_t datagram_build_fletcher_checksum(datagrizzle_t *d)
{
	uint16_t i = 0;
	uint16_t j = 0;
	uint8_t end;
	uint16_t s1 = 1;
	uint16_t s2 = 0;

	//Fletcher Computation
	end = datagram_get_payload_size(d);
	for(i = 0; i < end; i++)
	{
		s1 += d->payload.data[i];
		s1 %= 255;
		s2 += s1;
		s2 %= 255;
	}
	//Computed
	j = ((uint16_t)s2 * 256) + (uint16_t)s1;
	//printf("\tComputed Fletcher = 0x%X\n",j]);

	return j;
}

// **************************** Datagram processing functions *****************************

//Checks if the datagram is valid, which includes
//1) Checking the header is valid (XOR)
//2) Checking this is the corect destination by comparing the dest field and the bitMask
//3) Checking the payload is valid (CRC/Fletcher)
//If The header is valid and the datagram is destined for us (the bitmask destination matches)
//The payload is processed and the following information is filled out into the datagram.
//1) Lightweight Command Field
//2) Datagram Destination
//3) Payload Size
//Note that the contents of the payload are not copied accross here because that would be a
//wasted operation if the datagram specified a lightweight command.
//Returns the cmd (-1 if invalid or not addressed to us)
int16_t datagram_process(datagrizzle_t *d, uint16_t bitMask)
{
	uint8_t error = FALSE;

	//Fist check the header XOR CRC
	if(datagram_check_header(d))
	{
		//printf("Header OK\n");
		//Check if its addressed to us before we go any further
		if(datagram_get_destination(d) & bitMask)
		{
			//printf("Datagram is addressed OK\n");
			//Its addressed to us
			//Check the payload is valid (Fletcher)
			if(datagram_check_fletcher(d))
			{
				//printf("Fletcher OK\n")
				;//The datagram is valid
			}
			else
			{
				error = TRUE; //Payload Fail
				fprintf(stderr, "Payload Fletcher Checksum is wrong\n");
		  }
		}
		else
		{
			error = TRUE; //Not addressed to us
		}
	}
	else
	{
		error = TRUE; //Header Fail
  	fprintf(stderr, "Header CRC is wrong\n");
	}

	//Return -1 for error, or the command
	return (error ? -1 : datagram_get_command(d));
}

//Processes (Copies into the correct location, removing the size field) the payload data
//from the datagram buffer into the vars array at the addresses as specified within
//the payload address,size,data tuples
#define ADDRESS_STATE 0
#define SIZE_STATE 1
#define DATA_STATE 2
void datagram_process_payload(parsegram_t *p, datagrizzle_t *d, uint16_t startAddress, uint16_t endAddress)
{
	uint8_t state = 0;
	uint8_t tupleSize = 0;
	uint8_t i = 0;
	uint8_t index = 0;
	uint8_t endTuple = 0;
	uint8_t endPayload = datagram_get_payload_size(d);
	uint16_t address;

	//Lock the mutexs to prevent the receiving thread overwriting
	pthread_mutex_lock(&d->mutexProcessing);

	//Start by looking for an address
	state = ADDRESS_STATE;
	while(i < endPayload)
	{
		if(state == ADDRESS_STATE)
		{
			//Address sent [AddyHigh][AddyLo]
			address = 0;
			address |= (0xFF00 & (d->payload.data[i] << 8));
			address |= d->payload.data[i+1];
			//printf("\nAddr: 0x%X ",addy);

			state = SIZE_STATE;
			i+=2; //Skip over the 2nd address byte
		}
		else if(state == SIZE_STATE)
		{
			tupleSize = d->payload.data[i];
			//printf("Size: 0x%X Data: ",tupleSize);

			//Check the tuple is in the local address range
			if((address >= startAddress) && (address <= endAddress))
			{
				i++;
				state = DATA_STATE;
			}
			else //if not skip over this tuple
			{
				i += tupleSize + 1;
				state = ADDRESS_STATE;
			}
		}
		else if (state == DATA_STATE)
		{
			//This tuples data ends at
			endTuple = i + tupleSize;
      uint8_t *array = calloc(tupleSize, sizeof(uint8_t));

  		//Copy the data into the local variable buffer
			for(index = 0 ; index < tupleSize; i++, index++)
			{
				array[index] = d->payload.data[i];
			}

      // adding parsed pair "address-tuple" to parsegram
			parsegram_put(p, address, array);
			state = ADDRESS_STATE;
		}
		else
		{
  		printf("ERROR: Unknown state %i", state);
		}
	}
	//printf("\n");

	//Unlock the mutexs to prevent the receiving thread overwriting
	pthread_mutex_unlock(&d->mutexProcessing);
}

// **************************** Datagram printing functions *****************************

// Prints the payload information out
void payload_print(payload_t *p)
{
	uint8_t i, endPayload;

	printf("Payload:\n");
	printf("\tHeader:\n");

	for(i = 0; i <= DATAGRAM_HEADER_CHSUM; i++)
	{
		printf("[0x%X]", p->header[i]);
	}

	printf("\n\tData:\n");
	endPayload = p->header[DATAGRAM_PAYLOAD_SIZE];
	for(i = 0; i < endPayload; i++)
	{
		printf("[0x%X]", p->data[i]);
	}

	printf("\nDone!\n");
}

//Prints a datagram
void datagram_print(datagrizzle_t *d)
{
	uint8_t i;
	uint8_t endPayload;

	printf("Datagram Contents:\n");
	printf("\t[0x%X] Destination\n",datagram_get_destination(d));
	printf("\t[0x%X] Sender\n",datagram_get_sender(d));
	printf("\t[0x%X] Command\n",datagram_get_command(d));
	printf("\t[0x%X] Payload Size\n",datagram_get_payload_size(d));
	printf("\t[0x%X] Header Checksum\n",datagram_get_hchsum(d));
	printf("\t[0x%X] Payload Checksum\n",datagram_get_chsum(d));
	printf("\tBegin Payload: ");

	endPayload = datagram_get_payload_size(d);
	for(i = 0; i < endPayload; i++)
	{
		printf("[0x%X]", d->payload.data[i]);
	}

	printf("\n\tEnd Payload\n");
	printf("End Datagram\n");

}

// **************************** Datagram initialization functions *****************************

// Initialises the datagram
void datagram_init(datagrizzle_t *d, uint16_t maxSize)
{
	d->maxPayloadSize = maxSize;
	d->currentPayloadSize = 0;

	//Threading stuff
	pthread_mutex_init(&d->mutexRecieved, NULL);
	pthread_mutex_init(&d->mutexProcessing, NULL);
	d->recieved = FALSE;
	d->processing = FALSE;
	d->recieverStopped = FALSE;
	d->pauseMilliseconds = 0;

	//To prevent the callback from being called until its been inited
	d->received_callback = NULL;
	d->received_callback_user_data = NULL;
}

// clears datagram
void datagram_clear(datagrizzle_t *d)
{
	uint8_t i, sizeOfHeader;
	sizeOfHeader = DATAGRAM_HEADER_LEN;

  // clears header
	for(i = 0; i < sizeOfHeader; i++)
	{
		d->payload.header[i] = 0x0;
	}

	// clears data
	for(i = 0; i < DATAGRAM_MAX_PAYLOAD_SIZE; i++)
	{
		d->payload.data[i] = 0x0;
	}

	// reset fields
	d->currentPayloadSize = 0;
	d->recieved = FALSE;
	d->processing = FALSE;
	d->pauseMilliseconds = 0;
	d->receiveBitmask = 0;
	d->port = 0;
}

// **************************** Datagram ADD_TYPE functions *****************************

//adds a float to the datagram.
uint8_t datagram_add_float(datagrizzle_t *d, uint16_t address, float f)
{
	uint8_t i,len = 0;
	uint32_t asFloat;
	i = d->currentPayloadSize;
	len = sizeof(float);

	//Check it will fit in the buffer
	if(d->currentPayloadSize + 3 + len < d->maxPayloadSize)
	{
		//Store address [AddyHigh][AddyLow]
		d->payload.data[i++] = HI_BYTE(address);
		d->payload.data[i++] = LO_BYTE(address);

		//Store number of bytes
		d->payload.data[i++] = len;

		//Store the data
		asFloat = *((uint32_t *)&f);

		//printf("Data as float=%f, uint32 = 0x%X\n",f,asFloat);
		d->payload.data[i++] = LONG_HI_HI_BYTE(asFloat);
		d->payload.data[i++] = LONG_HI_BYTE(asFloat);
		d->payload.data[i++] = LONG_LO_BYTE(asFloat);
		d->payload.data[i++] = LONG_LO_LO_BYTE(asFloat);

		//update the payload struct to reflect how full the payload buffer is
		d->currentPayloadSize += len + 3;
		return TRUE;
	}
	else
		return FALSE;	//Data wont fit in buffer
}

//adds a byte to the datagram.
uint8_t datagram_add_byte(datagrizzle_t *d, uint16_t address, uint8_t b)
{
	uint8_t i,len = 0;
	i = d->currentPayloadSize;
	len = sizeof(uint8_t);

	//printf("Adding Byte=0x%X @ index %d, to address 0x%X\n",b,i,address);
	//Check it will fit in the buffer
	if(d->currentPayloadSize + 3 + len < d->maxPayloadSize)
	{
		//Store address
		d->payload.data[i++] = HI_BYTE(address);
		d->payload.data[i++] = LO_BYTE(address);

		//Store number of bytes
		d->payload.data[i++] = len;

		//Store the data
		d->payload.data[i] = b;

		//update the payload struct to reflect how full the payload buffer is
		d->currentPayloadSize += len + 3;
		return TRUE;
	}
	else
		return FALSE;	//Data wont fit in buffer
}

//adds a short to the datagram.
uint8_t datagram_add_short(datagrizzle_t *d, uint16_t address, uint16_t s)
{
	uint8_t i,len = 0;
	i = d->currentPayloadSize;
	len = sizeof(uint16_t);

	//Check it will fit in the buffer
	if(d->currentPayloadSize + 3 + len < d->maxPayloadSize)
	{
		//Store address
		d->payload.data[i++] = HI_BYTE(address);
		d->payload.data[i++] = LO_BYTE(address);

		//Store number of bytes
		d->payload.data[i++] = len;

		//Store the data
		d->payload.data[i++] = HI_BYTE(s);
		d->payload.data[i++] = LO_BYTE(s);

		//update the payload struct to reflect how full the payload buffer is
    d->currentPayloadSize += len + 3;
		return TRUE;
	}
	else
		return FALSE;	//Data wont fit in buffer
}

//adds a float to the datagram.
uint8_t datagram_add_int(datagrizzle_t *d, uint16_t address, uint32_t anInt)
{
	uint8_t i,len = 0;

	i = d->currentPayloadSize;
	len = sizeof(uint32_t);

	//Check it will fit in the buffer
	if(d->currentPayloadSize + 3 + len < d->maxPayloadSize)
	{
		//Store address [AddyHigh][AddyLow]
		d->payload.data[i++] = HI_BYTE(address);
		d->payload.data[i++] = LO_BYTE(address);

		//Store number of bytes
		d->payload.data[i++] = len;

		//Store the data
		d->payload.data[i++] = LONG_HI_HI_BYTE(anInt);
		d->payload.data[i++] = LONG_HI_BYTE(anInt);
		d->payload.data[i++] = LONG_LO_BYTE(anInt);
		d->payload.data[i++] = LONG_LO_LO_BYTE(anInt);

		//update the payload struct to reflect how full the payload buffer is
		d->currentPayloadSize += len + 3;
		return TRUE;
	}
	else
		return FALSE;	//Data wont fit in buffer
}

//adds a string to the datagram.
uint8_t datagram_add_string(datagrizzle_t *d, uint16_t address, char *s)
{
	uint8_t i,len = 0;
	i = d->currentPayloadSize;

	//Get the length of the string
	len = strlen(s) + 1; //Add 1 for null termination

	//Check it will fit in the buffer
	if(d->currentPayloadSize + 3 + len < d->maxPayloadSize)
	{
		//Store address
		d->payload.data[i++] = HI_BYTE(address);
		d->payload.data[i++] = LO_BYTE(address);

		//Store number of bytes
		d->payload.data[i++] = len;

		//Copy from s into the buffer (including the null char)
		strcpy(&d->payload.data[i],s);

		//update the payload struct to reflect how full the payload buffer is
		d->currentPayloadSize += len + 3;
		return TRUE;
	}
	else
		return FALSE;	//Data wont fit in buffer
}


// **************************** Datagram Sending functions *****************************


// Prepares a datagram for sending
// datagram must not be updated/reused before being send
uint8_t datagram_pack(datagrizzle_t *d)
{
	//Fill out the sync bytes
	d->payload.header[DATAGRAM_SYNC0] = DATAGRAM_SYNC0_BYTE;
	d->payload.header[DATAGRAM_SYNC1] = DATAGRAM_SYNC1_BYTE;
	//The dest and command and sender are already filled out providing
	//the getters and setters have been used

	//Fill out the payload size
	datagram_set_payload_size(d, d->currentPayloadSize);

	//Generate the complete checksum
	datagram_set_chsum(d, datagram_build_fletcher_checksum(d));

	//Header Checksum
	datagram_set_hchsum(d, datagram_build_header_checksum(d));

	d->currentPayloadSize = 0;
	return TRUE;
}

//Sends the datagram
void datagram_send(datagrizzle_t *d)
{
	uint32_t i;
	uint32_t j;
	uint32_t sizeOfHeader;
	uint32_t sizeOfPayload;
	uint32_t sizeOfDatagram;
	int sendStatus;

	sizeOfHeader = DATAGRAM_HEADER_LEN;
	sizeOfPayload = datagram_get_payload_size(d);

  // size of datagram = header size + payload data size
	sizeOfDatagram = sizeOfHeader + sizeOfPayload;
  uint8_t data[sizeOfDatagram];

  // copying data
  for (i = 0; i < sizeOfHeader; i++)
  {
    data[i] = d->payload.header[i];
  }

  for (j = 0; j < sizeOfPayload; j++,i++)
  {
    data[i] = d->payload.data[j];
  }

	//Send it
	sendStatus = multicast_send(UDP_DEFAULT_IP, d->port==0?UDP_DEFAULT_PORT:d->port, data, sizeOfDatagram);
	if (sendStatus < 0)
	{
		fprintf(stderr, "multicast_send returned error code %d\n", sendStatus);
	}
}

// **************************** Datagram Recieving functions *****************************

//Threaded Reciever Function
void *datagram_threaded_reciever(void *tsd)
{
	datagrizzle_t *d;
	int recvStatus;
	uint32_t i;
	uint32_t j;
	uint32_t sizeOfHeader;
	uint32_t sizeOfDatagram;

  // size of datagram = header size + max payload data size
	sizeOfHeader = DATAGRAM_HEADER_LEN;
	sizeOfDatagram = sizeOfHeader + DATAGRAM_MAX_PAYLOAD_SIZE;
	uint8_t data[sizeOfDatagram];

	//Assign the TSD
	d = (datagrizzle_t *)tsd;

	//Recieve Loop
	while(!d->recieverStopped)
	{
		//Check if the main thread is not busy processing data
		pthread_mutex_lock(&d->mutexProcessing);
		if(!d->processing)
		{
			//Having unlock here and at the base of the if means the main should block
			//for 1 cycle at max
			pthread_mutex_unlock(&d->mutexProcessing);

			//Block here waiting for UDP data
			int port = d->port;
			if(port== 0){
				port = UDP_DEFAULT_PORT;
			}
			recvStatus = multicast_recv(UDP_DEFAULT_IP, port, data, d->from);
			if (recvStatus < 0)
			{
				fprintf(stderr, "multicast_recv returned error code %d\n", recvStatus);
			}

			// copying data from received buffer to inner datagram fields
			for (i = 0; i < sizeOfHeader; i++)
			{
			d->payload.header[i] = data[i];
			}

			for (j = 0, i = sizeOfHeader; i < sizeOfDatagram; j++, i++)
			{
			d->payload.data[j] = data[i];
			}

			//Activate the callback with the processed datagram
			printf("Callback address = 0x%08X\n", d->received_callback);
			if(d->received_callback != NULL){
				(d->received_callback)(datagram_process(d, d->receiveBitmask), d->received_callback_user_data);
			}

			//FIXME: This can be removed when everything uses callbacks
			pthread_mutex_lock(&d->mutexRecieved);
			printf("Signaling Data Recieved\n");
			d->recieved = TRUE;
			pthread_mutex_unlock(&d->mutexRecieved);
		}
		else
		{
			//See comment above
			pthread_mutex_unlock(&d->mutexProcessing);
		}
	}
	printf("Reciever Stopped - Exiting\n");
	return NULL;
}

//The main processing function, suitable for insertion into an applications main loop
//Returns TRUE if a datagram was recieved and should be processed
uint8_t datagram_recieved_and_begin_processing(datagrizzle_t *d)
{
	uint8_t isOK;
	isOK = FALSE;

	pthread_mutex_lock(&d->mutexRecieved);
	if(d->recieved == TRUE)
	{
		d->recieved = FALSE;
		pthread_mutex_unlock(&d->mutexRecieved);
		//printf("Data Recieved Acknowledged\n");

		pthread_mutex_lock(&d->mutexProcessing);
		d->processing = TRUE;
		pthread_mutex_unlock(&d->mutexProcessing);
		//printf("Wait for Processing\n");

		isOK = TRUE;
	}
	else
	{
	  pthread_mutex_unlock(&d->mutexRecieved);
	}

	return isOK;
}

void datagram_end_processing(datagrizzle_t *d)
{
	pthread_mutex_lock(&d->mutexProcessing);
	d->processing = FALSE;
	pthread_mutex_unlock(&d->mutexProcessing);
	//printf("Processing Complete\n");
}

//Sets the maximum recieve frequency to prevent thread starvation
//Range 1-1000; 100-200 is optimum;
void datagram_set_recieve_frequency(datagrizzle_t *d, uint8_t hz)
{
	d->pauseMilliseconds = (uint8_t)((1.0/((float)hz))*1000.0);
	//printf("Pause for %d Milliseconds\n",d->pauseMilliseconds);
	d->time_ns.tv_sec = 0;
	d->time_ns.tv_nsec = d->pauseMilliseconds * 1000000;
}

//Executes the pause of length specified in set_recieve_frequency if
//enabled (if pauseMilliseconds != 0)
void datagram_pause_recieve(datagrizzle_t *d)
{
	if(d->pauseMilliseconds != 0)
		nanosleep(&d->time_ns, NULL);
}

void datagram_send_log_message(char *msg, uint16_t sender, int port)
{
	//Datagram for sending
	datagrizzle_t DgSend;

	//Initiation
	datagram_clear(&DgSend);	//Sender
	datagram_init(&DgSend,DATAGRAM_MAX_PAYLOAD_SIZE);
	datagram_set_destination(&DgSend,(LOGGING_D_BITMASK | COMM_D_BITMASK));
	datagram_set_command(&DgSend,LWC_LOGMSG);
	datagram_set_sender(&DgSend,sender);
	datagram_add_string(&DgSend,LOGGING_D_MESSAGE,msg);
	datagram_pack(&DgSend);
	DgSend.port = port==0?UDP_DEFAULT_PORT:port;

	datagram_send(&DgSend);
}

/**

// Prints the payload information out
void debug_print_buffer(uint8_t *buffer, uint32_t len)
{
	printf("\n\tDEBUG-BUFFER:\n");
	uint32_t i;
	printf("size: %d \n", len);

	for(i = 0; i < len; i++)
	{
		printf("[0x%X]", buffer[i]);
	}

	printf("\nDEBUG-BUFFER-DONE!\n");
}


*/

