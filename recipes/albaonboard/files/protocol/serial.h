#ifndef SERIAL_H
#define SERIAL_H
#include <termios.h>

extern int serial_configure(FILE *serial, speed_t baud_rate, char hwFlowControl);
#endif
