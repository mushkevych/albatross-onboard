#ifndef _UDP_MULTICAST_H_
#define _UDP_MULTICAST_H_

#define BUF_SIZE 1024
#define UDP_MULTICAST_TTL 8
#define UDP_DEFAULT_IP "239.192.1.100"
//#define UDP_DEFAULT_IP "224.0.0.1"
#define UDP_DEFAULT_PORT 50000

// Errors
#define ERR_SOCK_CREATE		-1
#define ERR_UNKNOWN_HOST	-2
#define ERR_SENDTO_FAILED	-3
#define ERR_JOIN_MULTICAST	-4
#define ERR_BIND_FAILED		-5
#define ERR_RECVFROM_FAILED	-6
#define ERR_DROP_MULTICAST	-7

#ifdef _CPP
extern "C"
{
#endif
int multicast_send(char *addr, int port, char *data, int len);
int multicast_recv(char *addr, int port, char *data, char *fromaddr);
#ifdef _CPP
}
#endif
#endif
