"""
   Pyrex file with common definitions
   Bohdan Mushkevych, 2009.
"""

#------------------------------------------------------------------------------
# External types we need
ctypedef unsigned short uint16_t
ctypedef unsigned char uint8_t
ctypedef short int16_t
ctypedef char int8_t
ctypedef int uint32_t
ctypedef int size_t

#------------------------------------------------------------------------------
# External functions we need
cdef extern from "stdlib.h":
  void* malloc(size_t size)
  void free(void* ptr)

cdef extern from "string.h":
  void* memset(void *s, int c, size_t n)

cdef extern from "Python.h":
  object PyString_FromStringAndSize(char *, int)

cdef extern from "pthread.h":
  ctypedef struct pthread_t:
    pass # (We don't need to access any of the members so we don't define them)
  ctypedef struct pthread_attr_t:
    pass 
  int pthread_create(pthread_t *thread, pthread_attr_t *attr, void *(*func)(void *), void *arg)
#------------------------------------------------------------------------------

