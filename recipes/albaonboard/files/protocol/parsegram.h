#ifndef _PARSEGRAM_H_
#define _PARSEGRAM_H_

#include <stdlib.h>   //For size_t definition
#include <inttypes.h> //For uintxx_t definitions

#ifdef _CPP
extern "C"
{
#endif // _CPP

#include "SCL_map.h"

#ifdef _CPP
}
#endif // _CPP
 
/**
 * structure holds parsed data from datagram.
 * data is stored as pair "key - value"
 * key is uint8_t address from protocol.h
 * value is uint8_t[] array, presenting byte, short, int, float or string
 *
 * incapsulate avionic data, and allow accessing it by name (address)
 */
typedef struct 
{
  SCL_map_t mapParsegram;
} parsegram_t;

#ifdef _CPP
extern "C"
{
#endif // _CPP

// **************************** Parsegram Init functions *****************************
/**
 * functions creates new parsegram
 */
void parsegram_init(parsegram_t* p);

/**
 * functions clears the parsegram, by removing all entries
 */
void parsegram_clear(parsegram_t* p);

/**
 * functions deletes the parsegram, by freeing its memory
 */
void parsegram_delete(parsegram_t* p); 
// **************************** Parsegram Access functions *****************************
/** 
 * function inserts elements into the parsegram
 * existing elements are overwritten
 */
void parsegram_put(parsegram_t* p, uint8_t key, uint8_t *value);
/** 
 * function removes element from parsegram
 * element is specified by its key
 */
void parsegram_remove(parsegram_t* p, uint8_t key);

/** 
 * function returns element from parsegram
 * element is specified by its key
 * if no element with given key is found, NULL is returned
 */
uint8_t* parsegram_get(parsegram_t* p, uint8_t key);

// **************************** Parsegram Conversions functions *****************************
/**
 * method inserts float value to the parsegram
 */
void parsegram_put_float(parsegram_t* p, uint8_t key, float value);
float parsegram_get_as_float(parsegram_t* p, uint8_t key);

/**
 * method inserts byte value to the parsegram
 */
void parsegram_put_byte(parsegram_t* p, uint8_t key, uint8_t value);
uint8_t parsegram_get_as_byte(parsegram_t* p, uint8_t key);

/**
 * method inserts short value to the parsegram
 */
void parsegram_put_short(parsegram_t* p, uint8_t key, uint16_t value);
uint16_t parsegram_get_as_short(parsegram_t* p, uint8_t key);

/**
 * method inserts integer value to the parsegram
 */
void parsegram_put_integer(parsegram_t* p, uint8_t key, uint32_t value);
uint32_t parsegram_get_as_integer(parsegram_t* p, uint8_t key);
 
/**
 * function return number of parsegram entries
 */
uint8_t parsegram_get_size(parsegram_t *p);
 
/**
 * function return array of keys
 */
uint8_t* parsegram_get_keys(parsegram_t *p);
#ifdef _CPP
}
#endif //_CPP



#endif //__PARSEGRAM_H_
