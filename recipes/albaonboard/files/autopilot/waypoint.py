'''
this Python module presents Waypoints
A position in the air, that UAV should fly over

Waypoint is specified by ID (for readability purposes);
latitude, longtitude and altitude - specifying the position in the air
and LWC - command that needs to be performed by UAV as it reaches the Waypoint 
(for example - perform Photo Camera Shot)

@date: Apr 24, 2010
@author: Bohdan Mushkevych
'''

class Waypoint:
    def __init__(self, _id = 0, latitude = 0, longtitude = 0, altitude = 0, lwc = 0):
        self._id = _id
        self.latitude = latitude
        self.longtitude = longtitude
        self.altitude = altitude
        self.lwc = lwc #lwc stands for "light weight command"
        
    def __str__ (self):
        return "id=%d \t latitude=%f \t longtitude=%f \t altitude=%f \t lwc=%d" % (self._id, self.latitude, self.longtitude, self.altitude, self.lwc)
        
    def __eq__(self, other):
        if isinstance(other, Waypoint) \
            and self._id == other._id \
            and self.latitude == other.latitude \
            and self.longtitude == other.longtitude \
            and self.altitude == other.altitude \
            and self.lwc == other.lwc:
            return True
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)
        
