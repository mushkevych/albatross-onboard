/*
 * Original File Copyright (C) 2003  Pascal Brisset, Antoine Drouin
 *
 * This file was from paparazzi (http://www.nongnu.org/paparazzi).
 * Distributed under GPLv2.
 *
 * Modified for Albatross, By John Stowers and Hugo Vincent
 */

#ifndef _GPS_H_
#define _GPS_H_

struct GPSData_t {
	float latitude;
	float longitude;
	float altitude;
	float UTMNorth;
	float UTMEast;
	float UTMAltitude;
	float ECEFx;
	float ECEFy;
	float ECEFz;
	float ECEFvelx;
	float ECEFvely;
	float ECEFvelz;
	// Other stuff
	uint8_t mode;
	uint32_t timeOfWeek; // ms
	float gspeed; // m/s
	float climb; // m/s
	float heading; // degree clockwise from North
	uint8_t satsInView;
	uint8_t msgReceived;
	
};
	
//extern uint8_t	gps_mode;
//extern uint32_t	gps_itow;    /* ms	*/
//extern float	gps_falt;    /* m	*/
//extern float	gps_fspeed;  /* m/s	*/
//extern float	gps_fclimb;  /* m/s	*/
//extern float	gps_fcourse; /* rad	*/
//extern int32_t	gps_utm_east, gps_utm_north;
//extern float	gps_east, gps_north; /* m	*/

void parse_ubx(struct GPSData_t *g, uint8_t ch);
void parse_gps_msg(struct GPSData_t *g);
FILE *gps_init(char *serial_port);
void print_gps(struct GPSData_t *g);

#define GPS_BAUD_RATE B57600
#define GPS_SERIAL_PORT "/dev/ttyS2"


//#define GPS_FIX_VALID(gps_mode) (gps_mode == 3)

// From flight_path.h: shouldn't really be here.
#define NAV_UTM_EAST0 364222
#define NAV_UTM_NORTH0 4788551

/* Generated from conf/ubx.xml */
/* Please DO NOT EDIT */

#define UBX_SYNC1 0xB5
#define UBX_SYNC2 0x62

#define UBX_NAV_ID 0x01

#define UBX_NAV_POSLLH_ID 0x02
#define UBX_NAV_POSLLH_ITOW(_ubx_payload) (*((uint32_t*)(_ubx_payload+0)))
#define UBX_NAV_POSLLH_LON(_ubx_payload) (*((int32_t*)(_ubx_payload+4)))
#define UBX_NAV_POSLLH_LAT(_ubx_payload) (*((int32_t*)(_ubx_payload+8)))
#define UBX_NAV_POSLLH_HEIGHT(_ubx_payload) (*((int32_t*)(_ubx_payload+12)))
#define UBX_NAV_POSLLH_HMSL(_ubx_payload) (*((int32_t*)(_ubx_payload+16)))
#define UBX_NAV_POSLLH_Hacc(_ubx_payload) (*((uint32_t*)(_ubx_payload+20)))
#define UBX_NAV_POSLLH_Vacc(_ubx_payload) (*((uint32_t*)(_ubx_payload+24)))

#define UBX_NAV_POSUTM_ID 0x08
#define UBX_NAV_POSUTM_ITOW(_ubx_payload) (*((uint32_t*)(_ubx_payload+0)))
#define UBX_NAV_POSUTM_EAST(_ubx_payload) (*((int32_t*)(_ubx_payload+4)))
#define UBX_NAV_POSUTM_NORTH(_ubx_payload) (*((int32_t*)(_ubx_payload+8)))
#define UBX_NAV_POSUTM_ALT(_ubx_payload) (*((int32_t*)(_ubx_payload+12)))
#define UBX_NAV_POSUTM_ZONE(_ubx_payload) (*((int8_t*)(_ubx_payload+16)))
#define UBX_NAV_POSUTM_HEM(_ubx_payload) (*((int8_t*)(_ubx_payload+17)))

#define UBX_NAV_SOL_ID 0x06
#define UBX_NAV_SOL_ECEF_X(_ubx_payload) (*((int32_t*)(_ubx_payload+12)))
#define UBX_NAV_SOL_ECEF_Y(_ubx_payload) (*((int32_t*)(_ubx_payload+16)))
#define UBX_NAV_SOL_ECEF_Z(_ubx_payload) (*((int32_t*)(_ubx_payload+20)))
#define UBX_NAV_SOL_ECEFVX(_ubx_payload) (*((int32_t*)(_ubx_payload+28)))
#define UBX_NAV_SOL_ECEFVY(_ubx_payload) (*((int32_t*)(_ubx_payload+32)))
#define UBX_NAV_SOL_ECEFVZ(_ubx_payload) (*((int32_t*)(_ubx_payload+36)))
#define UBX_NAV_SOL_numSV(_ubx_payload) (*((uint8_t*)(_ubx_payload+47)))

#define UBX_NAV_STATUS_ID 0x03
#define UBX_NAV_STATUS_ITOW(_ubx_payload) (*((uint32_t*)(_ubx_payload+0)))
#define UBX_NAV_STATUS_GPSfix(_ubx_payload) (*((uint8_t*)(_ubx_payload+4)))
#define UBX_NAV_STATUS_Flags(_ubx_payload) (*((uint8_t*)(_ubx_payload+5)))
#define UBX_NAV_STATUS_DiffS(_ubx_payload) (*((uint8_t*)(_ubx_payload+6)))
#define UBX_NAV_STATUS_res(_ubx_payload) (*((uint8_t*)(_ubx_payload+7)))
#define UBX_NAV_STATUS_TTFF(_ubx_payload) (*((uint32_t*)(_ubx_payload+8)))
#define UBX_NAV_STATUS_MSSS(_ubx_payload) (*((uint32_t*)(_ubx_payload+12)))

#define UBX_NAV_VELNED_ID 0x12
#define UBX_NAV_VELNED_ITOW(_ubx_payload) (*((uint32_t*)(_ubx_payload+0)))
#define UBX_NAV_VELNED_VEL_N(_ubx_payload) (*((int32_t*)(_ubx_payload+4)))
#define UBX_NAV_VELNED_VEL_E(_ubx_payload) (*((int32_t*)(_ubx_payload+8)))
#define UBX_NAV_VELNED_VEL_D(_ubx_payload) (*((int32_t*)(_ubx_payload+12)))
#define UBX_NAV_VELNED_Speed(_ubx_payload) (*((uint32_t*)(_ubx_payload+16)))
#define UBX_NAV_VELNED_GSpeed(_ubx_payload) (*((uint32_t*)(_ubx_payload+20)))
#define UBX_NAV_VELNED_Heading(_ubx_payload) (*((int32_t*)(_ubx_payload+24)))
#define UBX_NAV_VELNED_SAcc(_ubx_payload) (*((uint32_t*)(_ubx_payload+28)))
#define UBX_NAV_VELNED_CAcc(_ubx_payload) (*((uint32_t*)(_ubx_payload+32)))

#define UBX_NAV_SVINFO_ID 0x30
#define UBX_NAV_SVINFO_ITOW(_ubx_payload) (*((uint32_t*)(_ubx_payload+0)))
#define UBX_NAV_SVINFO_NCH(_ubx_payload) (*((uint8_t*)(_ubx_payload+4)))
#define UBX_NAV_SVINFO_RES1(_ubx_payload) (*((uint8_t*)(_ubx_payload+5)))
#define UBX_NAV_SVINFO_RES2(_ubx_payload) (*((uint8_t*)(_ubx_payload+6)))
#define UBX_NAV_SVINFO_chn(_ubx_payload,_ubx_block) (*((uint8_t*)(_ubx_payload+7+12*_ubx_block)))
#define UBX_NAV_SVINFO_SVID(_ubx_payload,_ubx_block) (*((uint8_t*)(_ubx_payload+8+12*_ubx_block)))
#define UBX_NAV_SVINFO_Flags(_ubx_payload,_ubx_block) (*((uint8_t*)(_ubx_payload+9+12*_ubx_block)))
#define UBX_NAV_SVINFO_QI(_ubx_payload,_ubx_block) (*((int8_t*)(_ubx_payload+10+12*_ubx_block)))
#define UBX_NAV_SVINFO_CNO(_ubx_payload,_ubx_block) (*((uint8_t*)(_ubx_payload+11+12*_ubx_block)))
#define UBX_NAV_SVINFO_Elev(_ubx_payload,_ubx_block) (*((int8_t*)(_ubx_payload+12+12*_ubx_block)))
#define UBX_NAV_SVINFO_Azim(_ubx_payload,_ubx_block) (*((int16_t*)(_ubx_payload+13+12*_ubx_block)))
#define UBX_NAV_SVINFO_PRRes(_ubx_payload,_ubx_block) (*((int32_t*)(_ubx_payload+15+12*_ubx_block)))

#define UBX_USR_ID 0x40

#define UBX_USR_IRSIM_ID 0x01
#define UBX_USR_IRSIM_ROLL(_ubx_payload) (*((int16_t*)(_ubx_payload+0)))
#define UBX_USR_IRSIM_PITCH(_ubx_payload) (*((int16_t*)(_ubx_payload+2)))

#define UBX_USR_SERVOS_ID 0x02
#define UBX_USR_SERVOS_N(_ubx_payload) (*((int8_t*)(_ubx_payload+0)))
#define UBX_USR_SERVOS_WIDTH(_ubx_payload,_ubx_block) (*((uint16_t*)(_ubx_payload+1+2*_ubx_block)))

#define UBX_USR_PPM_ID 0x03
#define UBX_USR_PPM_N(_ubx_payload) (*((int8_t*)(_ubx_payload+0)))
#define UBX_USR_PPM_WIDTH(_ubx_payload,_ubx_block) (*((uint16_t*)(_ubx_payload+1+2*_ubx_block)))

#endif
