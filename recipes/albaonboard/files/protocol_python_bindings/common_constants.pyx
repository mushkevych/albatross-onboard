"""
   Pyrex file with common constant definitions
   Bohdan Mushkevych, 2009.
"""

#------------------------------------------------------------------------------
# Includes

include "protocol.pyx"

#------------------------------------------------------------------------------
# Constants (the macros in datagram.h; some have DATAGRAM_ prefix removed)
MAX_PAYLOAD_SIZE = 200
UDP_FROM_STRING = 30

# Constants (the macros in udp_multicast.h)
BUF_SIZE = 1024
DEFAULT_IP = "239.192.1.100"
#DEFAULT_IP = "224.0.0.1"
DEFAULT_PORT = 50000

