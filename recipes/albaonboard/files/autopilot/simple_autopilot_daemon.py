# -*- coding: utf-8 -*-
'''
Script to run autopilot from console in a daemon mode

Created on July 4, 2010
@author: Bohdan Mushkevych
'''
from autopilot import Autopilot

if __name__ == "__main__":
  	# start thread
	pilot = Autopilot()
	pilot.start()
	print "AUTOPILOT DAEMON STARTED"
	print "Use Groundstation to setup Route and to switch it to Auto mode"  
