/*------- Albatross UAV - Hardware Interface Daemon -------------------------*
 *
 *	File: hwd.c
 *	Authors: John Stowers, Hugo Vincent
 *	Description: Handles acces to the hardware such as
 *	reading the IMU sensors (over the FPGA<->CPU memory interface, the
 *	serial port, or both).
 *
 *  Copyright (C) 2005  John Stowers, Hugo Vincent
 *	The code was from Albatross (http://johnstowers.no-ip.com).
 *	Distributed under GPL.
 */

/*-------Includes------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include "gps.h"
#include "hwd.h"
#include "../protocol/albatross_std.h"
#include "../protocol/serial.h"
//#include "../stated/stated.h"
#include "../protocol/udp_multicast.h"
#include "../protocol/datagram.h"
#include "../protocol/protocol.h"

#define SCALE_TO_RANGE(omin,omax,oval,newmin,newmax) (((oval/(omax-omin))*(newmax-newmin))+(((newmax-newmin)/2)+newmin))


/*-------Globals-------------------------------------------------------------*/
volatile unsigned int *fpga = 0;

uint8_t gpsRunning;
// adc stands for Air Data Computer
uint8_t adcRunning;

//FIXME: IMUTemp could be in SensorDataBuff
float IMUTemp; 	//Temperature from the onboard gyro temp sensor
struct SensorData_t SensorDataBuff;
struct GPSData_t GPSDataBuff;
FILE *gps_serial;
uint8_t running;
uint8_t setOutput;

// GPS Mode can be in three states: 0x00 - no lock, 0x01 - no lock, 0x02 - 2D lock, 0x03 - 3D lock
// stores the GPS mode from latest measurement
uint8_t gpsOldMode;

//Flags determining if the controllers are enabled or not
//If false then the signal passes straight through (i.e. part manual mode)
uint8_t yawDamperEnabled = FALSE;
uint8_t pitchDamperEnabled = FALSE;
uint8_t airspeedControllerEnabled = FALSE;
uint8_t altitudeControllerEnabled = FALSE;
uint8_t bankAngleControllerEnabled = FALSE;

//Threading stuff
pthread_mutex_t GPSLock;
pthread_t GPSThread;
uint8_t adcReadsGps, adcReadsComms;

//Datagram for sending
datagrizzle_t DgSend;

//Datagram for recieving
datagrizzle_t DgRecv;

//Parsegram
parsegram_t parsegram;

/*-------Function Prototypes-------------------------------------------------*/
void *hwd_read_gps(void *tsd);
void hwd_build_gps_packet(datagrizzle_t *d, struct GPSData_t *g);
void hwd_build_imu_packet(datagrizzle_t *d, struct SensorData_t *s);
void configure_loop_timer(int timerTime); // in microseconds
void datagram_callback(int cmd, void *user_data);
void timer_handler(int signum);

/*-------Main Function-------------------------------------------------------*/
int main(int argc, char **argv)
{
	pthread_t rcv;
	//Main loop sleep timer
	struct timespec timer;

#ifdef ARM
	//Set up the GPS
	gps_serial = gps_init(GPS_SERIAL_PORT);
	if(gps_serial == NULL)
	{
		albatross_error("Could not open serial port, exiting.", HW_D_BITMASK, ALBA_ERROR);
		exit(1);
	}
#else
	albatross_error("Running on PC", HW_D_BITMASK, ALBA_INFO);
#endif

	//Notify the logging daemon
	albatross_error("HwD Started.", HW_D_BITMASK, ALBA_INFO);

	//Initialise the Datagram Reciever
	datagram_clear(&DgRecv);
	datagram_init(&DgRecv, DATAGRAM_MAX_PAYLOAD_SIZE);

	//Initialise the datagram callbacks (logd recieves from all)
	datagram_set_receive_bitmask(&DgRecv, HW_D_BITMASK);
	datagram_set_callback(&DgRecv, datagram_callback, NULL);

	//Sender
	datagram_clear(&DgSend);
	datagram_init(&DgSend, DATAGRAM_MAX_PAYLOAD_SIZE);
	parsegram_init(&parsegram);

	//Init the mutexes for the GPS readings
	pthread_mutex_init(&GPSLock, NULL);

	//Start the GPS thread
	gpsRunning = TRUE; gpsOldMode = 0x00;
	pthread_create(&GPSThread, NULL, &hwd_read_gps, (void *)&GPSDataBuff);

	//Start the Datagram reciever thread
	pthread_create(&rcv, NULL, &datagram_threaded_reciever, (void *)&DgRecv);

	//Install the timer handler
	adcRunning = TRUE;
	configure_loop_timer(SAMPLE_RATE_USEC);

	//Initialise the main loop
	timer.tv_sec = 0;
	timer.tv_nsec = 10000000; //FIXME: Should the main loop sleep timers be in protocol.h?
	running = TRUE;
	setOutput = FALSE;
	while(running)
	{
		nanosleep(&timer,NULL);
		if(setOutput)
		{
			//FIXME: Magic number 1
			//float hw_d_servo0 = parsegram_get_as_float(&parsegram, HW_D_SERVO0);
			//fpga[12] = 0x80000000 + SCALE_TO_RANGE(-99.0, 99.0, -1*(int16_t) hw_d_servo0, 0, 1023);
			setOutput = FALSE;
		}
	}

	//FIXME: should join the threads and uninit the mutexes and stuff
	albatross_error("HwD Stopped.", HW_D_BITMASK, ALBA_INFO);

	return 0;
}

/*-------Implementation: Functions-------------------------------------------*/
void hwd_build_gps_packet(datagrizzle_t *d, struct GPSData_t *g)
{
#if SEND_LLH
	//GPS Info (Lat, Long, Altitude)
	datagram_add_float(d,STATE_D_LATITUDE,g->latitude);
	datagram_add_float(d,STATE_D_LONGITUDE,g->longitude);
	datagram_add_float(d,STATE_D_ALTITUDE,g->altitude);
#endif
#if SEND_ECEF
	//GPS Info (ECEF)
	datagram_add_float(d,STATE_D_XW,g->ECEFx);
	datagram_add_float(d,STATE_D_YW,g->ECEFy);
	datagram_add_float(d,STATE_D_ZW,g->ECEFz);
	datagram_add_float(d,STATE_D_XW_DOT,g->ECEFvelx);
	datagram_add_float(d,STATE_D_YW_DOT,g->ECEFvely);
	datagram_add_float(d,STATE_D_ZW_DOT,g->ECEFvelz);
#endif
	//Other Fun Stuff
	//FIXME: No Airspeed from GPS
	datagram_add_float(d,STATE_D_VELOCITY_AIRSPEED,g->gspeed);
	datagram_add_float(d,STATE_D_VELOCITY_GROUND,g->gspeed);
	datagram_add_byte(d,STATE_D_GPS_MODE,g->mode);
	datagram_add_byte(d,STATE_D_SATS_IN_VIEW,g->satsInView);
	datagram_add_float(d,STATE_D_HEADING,g->heading);
}

void hwd_build_imu_packet(datagrizzle_t *d, struct SensorData_t *s)
{
	//Data from the accelerometers
	datagram_add_float(d,STATE_D_U_DOT,s->acc[X]); //Local acceleration in local x (+ve forward)
	datagram_add_float(d,STATE_D_V_DOT,s->acc[Y]); //Local acceleration in local y (+ve right)
	datagram_add_float(d,STATE_D_W_DOT,s->acc[Z]); //Local acceleration in local z (+ve down)
	//Data from the gyroscopes
	datagram_add_float(d,STATE_D_P,s->gyr[Roll]); //Roll rotational velocity about local X (+ve right wing down)
	datagram_add_float(d,STATE_D_Q,s->gyr[Pitch]); //Pitch rotational velocity about local Y (+ve nose up)
	datagram_add_float(d,STATE_D_R,s->gyr[Yaw]); //Heading rotational velocity about local Z (+ve nose right)
	// IMU temp
	datagram_add_float(d,STATE_D_IMU_TEMP,IMUTemp); // FIXME this doesn't need to be fast
}

//Reads data from the GPS in a seperate thread
void *hwd_read_gps(void *tsd)
{
	struct GPSData_t *g;

	g = (struct GPSData_t *)tsd;
	while(gpsRunning)
	{
#ifdef ARM
		//No sleep needed on platform cause blocking fgetc
		parse_ubx(g, fgetc(gps_serial));
		if (g->msgReceived)
		{
			//Atomically process the data
			pthread_mutex_lock(&GPSLock);
			parse_gps_msg(g);
			pthread_mutex_unlock(&GPSLock);
			//print_gps(g);
			g->msgReceived = FALSE;
		}
#else
		//Simulates the slow update rate of the GPS
		sleep(1);
		pthread_mutex_lock(&GPSLock);
		pthread_mutex_unlock(&GPSLock);
#endif
	}
	return NULL; // Keep the compiler happy
}

void timer_handler(int signum)
{
#ifdef ARM
	int tempi; // temporary integer for conversions
#endif

	if(adcRunning)
	{
#ifdef ARM
/*
  	//COMMENTED: as we do not have other than GPS modules
		// Conversion to real units, and Calibration (First order... see stated.h)
		tempi = (int)(FPGA_ADC7 & FPGA_ADC_BITMASK) - ADC_CENTER;
		SensorDataBuff.acc[X] = (float)tempi * ((1.0/QUANT_PER_GEE) * FG * ACC_SCALE_X) - ACC_OFFSET_X;

		tempi = (int)(FPGA_ADC6 & FPGA_ADC_BITMASK) - ADC_CENTER;
		SensorDataBuff.acc[Y] = (float)tempi * ((1.0/QUANT_PER_GEE) * FG * ACC_SCALE_Y) - ACC_OFFSET_Y;

		tempi = (int)(FPGA_ADC0 & FPGA_ADC_BITMASK) - ADC_CENTER;
		SensorDataBuff.acc[Z] = (float)tempi * ((1.0/QUANT_PER_GEE) * FG * ACC_SCALE_Z) - ACC_OFFSET_Z; // negative because of the way the accelerometers are positioned in the plane

		tempi = (int)(FPGA_ADC1 & FPGA_ADC_BITMASK) - ADC_CENTER;
		SensorDataBuff.gyr[Pitch] = (float)tempi * ((1.0/QUANT_PER_RAD) * GYRO_SCALE_PITCH) - GYRO_OFFSET_PITCH;

		tempi = (int)(FPGA_ADC3 & FPGA_ADC_BITMASK) - ADC_CENTER;
		SensorDataBuff.gyr[Roll] = (float)tempi * ((1.0/QUANT_PER_RAD) * GYRO_SCALE_ROLL) - GYRO_OFFSET_ROLL;

		tempi = (int)(FPGA_ADC5 & FPGA_ADC_BITMASK) - ADC_CENTER;
		SensorDataBuff.gyr[Yaw] = (float)tempi * ((1.0/QUANT_PER_RAD) * GYRO_SCALE_YAW) - GYRO_OFFSET_YAW;

		IMUTemp = (float)((int)(FPGA_ADC4 & FPGA_ADC_BITMASK) - ZERO_DEG_C)/ QUANT_PER_KELVIN;
*/
#endif
		//COMMENTED: as we do not have other than GPS modules
		//hwd_build_imu_packet(&DgSend,&SensorDataBuff);

#if 1
		// Debugging
		printf("%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",
										SensorDataBuff.acc[X],
										SensorDataBuff.acc[Y],
										SensorDataBuff.acc[Z],
										SensorDataBuff.gyr[Pitch],
										SensorDataBuff.gyr[Roll],
										SensorDataBuff.gyr[Yaw],
										IMUTemp,
										GPSDataBuff.latitude,
										GPSDataBuff.longitude,
										GPSDataBuff.altitude,
										GPSDataBuff.heading,
										GPSDataBuff.mode,
										GPSDataBuff.gspeed);
#endif

		//Effectively ZOH the gps data at a rate slower than the ADC rate
		//ZOH - Zero-order hold - http://en.wikipedia.org/wiki/ZOH
		adcReadsGps++;
		if(adcReadsGps == NUM_SAMPLES_PER_GPS)
		{
			adcReadsGps = 0;
			//Will block here if the GPS thread is copying into the
			//GPS buffer at this point in time
			pthread_mutex_lock(&GPSLock);
			hwd_build_gps_packet(&DgSend, &GPSDataBuff);
			pthread_mutex_unlock(&GPSLock);

			//Let the Groundstation know that GPS mode has changed.
			if(GPSDataBuff.mode != gpsOldMode)
			{
				gpsOldMode = GPSDataBuff.mode;
				//Send a message to the groundstation
				if(GPSDataBuff.mode == 0x02)
				{
					albatross_error("GPS got 2D fix.", HW_D_BITMASK, ALBA_INFO);
				}
				if(GPSDataBuff.mode == 0x03)
				{
					albatross_error("GPS got 3D fix.", HW_D_BITMASK, ALBA_INFO);
				}
			}

		}

		//Now send the datagram
		adcReadsComms++;
		if (adcReadsComms == NUM_SAMPLES_PER_COMMD)
		{
			adcReadsComms = 0;
			datagram_set_destination(&DgSend, STATE_D_BITMASK | COMM_D_BITMASK);
		}
		else
		{
			datagram_set_destination(&DgSend, STATE_D_BITMASK);
		}
		datagram_set_command(&DgSend, LWC_DATA);
		datagram_set_sender(&DgSend, HW_D_BITMASK);
		datagram_pack(&DgSend);
		datagram_send(&DgSend);
	}

}

void configure_loop_timer(int timerTime) // in microseconds
{
	//Regulate the loop speed
	static struct sigaction sa;
	static struct itimerval timer;

	//Install timer_handler as the signal handler for SIGVTALRM.
	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = &timer_handler;
	sigaction(SIGALRM, &sa, NULL);

	//Configure the timer to expire after timerTime...
	timer.it_value.tv_sec = 0;
	timer.it_value.tv_usec = timerTime;
	//... and every timerTime msec after that.
	timer.it_interval.tv_sec = 0;
	timer.it_interval.tv_usec = timerTime;
	//Start a virtual timer. It counts down whenever this process is executing.
	setitimer(ITIMER_REAL, &timer, NULL);
}

void datagram_callback(int cmd, void *user_data)
{
	static char s[MAX_MESSAGE_LENGTH];

	if(cmd != -1)
	{
		if(cmd == LWC_HWD_EXIT)
		{
			gpsRunning = FALSE;
			adcRunning = FALSE;
			running = FALSE;
		}
		else if(cmd == LWC_ENABLE_DISABLE_CONTROLLERS)
		{
			//Process the command
			datagram_process_payload(&parsegram, &DgRecv, STATE_D_ADDR_START, STATE_D_ADDR_END);

			//Now selectively enable or disable the controllers
			yawDamperEnabled = ( parsegram_get_as_byte(&parsegram, STATE_D_YAW_ENABLED) == TRUE ? TRUE : FALSE);
			pitchDamperEnabled = ( parsegram_get_as_byte(&parsegram, STATE_D_PITCH_ENABLED) == TRUE ? TRUE : FALSE);
			altitudeControllerEnabled = ( parsegram_get_as_byte(&parsegram, STATE_D_ALTITUDE_ENABLED) == TRUE ? TRUE : FALSE);
			airspeedControllerEnabled = ( parsegram_get_as_byte(&parsegram, STATE_D_AIRSPEED_ENABLED) == TRUE ? TRUE : FALSE);
			bankAngleControllerEnabled = ( parsegram_get_as_byte(&parsegram, STATE_D_BANK_ENABLED) == TRUE ? TRUE : FALSE);

			//Generate a pretty message for datagram_send_log_message
			sprintf(s,"Controllers Enabled YD:%c PD:%c AL:%c AS:%c BA:%c",
					(yawDamperEnabled ? 'Y' : 'N'),
					(pitchDamperEnabled ? 'Y' : 'N'),
					(altitudeControllerEnabled ? 'Y' : 'N'),
					(airspeedControllerEnabled ? 'Y' : 'N'),
					(bankAngleControllerEnabled ? 'Y' : 'N'));
			datagram_send_log_message(s,HW_D_BITMASK,0);
		}
		else if(cmd == LWC_DATA)
		{
			//Process the datagram
			if(datagram_get_sender(&DgRecv) == STATE_D_BITMASK)
			{
				//FIXME: This should be protected by a mutex
				setOutput = TRUE;
				datagram_process_payload(&parsegram, &DgRecv, HW_D_ADDR_START, HW_D_ADDR_END);
			}
		}

	}
}
