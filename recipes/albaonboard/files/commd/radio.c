#include "../protocol/albatross_std.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include "radio.h"
#include "../protocol/serial.h"

int radio_enter_AT_mode(FILE *serial)
{
	uint8_t i;
	uint8_t send[] = ENTER_AT_COMMAND_MODE;
	uint8_t expectedRecv[] = ENTER_AT_COMMAND_MODE_RESPONSE;
	uint8_t recv[sizeof(expectedRecv)];

	if(radio_send_raw_command(serial,send,sizeof(send)))
	{
		DEBUG_RADIO_MSG("Sent Enter AT Command OK");
		if(radio_receive_raw_command(serial,recv,sizeof(recv)))
		{
			i = memcmp(expectedRecv,recv,sizeof(expectedRecv));
			if(i == 0)
			{
				DEBUG_RADIO_MSG("Response OK");
				return 1;
			}
			else
			{
				DEBUG_RADIO_MSG("Response FAIL");
				return -1;
			}
		}
		else
			return -1;
	}
	else
		return -1;

}

int radio_exit_AT_mode(FILE *serial)
{
	uint8_t i;
	uint8_t send[] = EXIT_AT_COMMAND_MODE;
	uint8_t expectedRecv[] = EXIT_AT_COMMAND_MODE_RESPONSE;
	uint8_t recv[sizeof(expectedRecv)];

	if(radio_send_raw_command(serial,send,sizeof(send)))
	{
		DEBUG_RADIO_MSG("Sent Exit AT Command OK");
		if(radio_receive_raw_command(serial,recv,sizeof(recv)))
		{
			i = memcmp(expectedRecv,recv,sizeof(expectedRecv));
			if(i == 0)
			{
				DEBUG_RADIO_MSG("Response OK");
				return 1;
			}
			else
			{
				DEBUG_RADIO_MSG("Response FAIL");
				return -1;
			}
		}
		else
			return -1;
	}
	else
		return -1;

}

//-ve for error
int radio_get_status(FILE *serial, uint8_t *firmwareVersion, uint8_t *status)
{
	uint8_t send[] = STATUS_REQUEST;
	uint8_t expectedRecv[] = STATUS_REQUEST_RESPONSE;
	uint8_t recv[sizeof(expectedRecv)];
	int res;

	DEBUG_RADIO_MSG("GETTING STATUS");
	//Enter AT Mode
	if(radio_enter_AT_mode(serial) > 0)
	{
		if(radio_send_raw_command(serial,send,sizeof(send)))
		{
			DEBUG_RADIO_MSG("Sent Status Command OK");
			if(radio_receive_raw_command(serial,recv,sizeof(recv)))
			{
				DEBUG_RADIO_MSG("Response OK");
				*status = recv[2];
				*firmwareVersion = recv[1];
				res = 1;
			}
			else
				res = -1;
		}
		else
			res = -1;

		//Exit AT mode
		if(radio_exit_AT_mode(serial)<0)
			DEBUG_RADIO_MSG("Could not Exit AT Mode");
	}
	else
	{
		DEBUG_RADIO_MSG("Could not Enter AT Mode");
		res = -1;
	}

	//Done
	return res;
}



int8_t radio_get_last_rssi(FILE *serial, uint8_t *rssi)
{
	uint8_t send[] = REPORT_LAST_VALID_RSSI;
	uint8_t expectedRecv[] = REPORT_LAST_VALID_RSSI_RESPONSE;
	uint8_t recv[sizeof(expectedRecv)];
	int res;

	DEBUG_RADIO_MSG("GETTING RSSI");
	//Enter AT Mode
	if(radio_enter_AT_mode(serial) > 0)
	{
		if(radio_send_raw_command(serial,send,sizeof(send)))
		{
			DEBUG_RADIO_MSG("Sent RSSI Command OK");
			if(radio_receive_raw_command(serial,recv,sizeof(recv)))
			{
				DEBUG_RADIO_MSG("Response OK");
				*rssi = recv[1];
				res = 1;
			}
			else
				res = -1;
		}
		else
			res = -1;

		//Exit AT mode
		if(radio_exit_AT_mode(serial)<0)
			DEBUG_RADIO_MSG("Could not Exit AT Mode");
	}
	else
	{
		DEBUG_RADIO_MSG("Could not Enter AT Mode");
		res = -1;
	}

	//Done
	return res;

}

int8_t radio_read_temperature(FILE *serial, uint8_t *temp)
{
	uint8_t send[] = READ_TEMPERATURE;
	uint8_t expectedRecv[] = READ_TEMPERATURE_RESPONSE;
	uint8_t recv[sizeof(expectedRecv)];
	int res;

	DEBUG_RADIO_MSG("GETTING TEMP");
	//Enter AT Mode
	if(radio_enter_AT_mode(serial) > 0)
	{
		if(radio_send_raw_command(serial,send,sizeof(send)))
		{
			DEBUG_RADIO_MSG("Sent Temperature Command OK");
			if(radio_receive_raw_command(serial,recv,sizeof(recv)))
			{
				DEBUG_RADIO_MSG("Response OK");
				*temp = recv[1];
				res = 1;
			}
			else
				res = -1;
		}
		else
			res = -1;

		//Exit AT mode
		if(radio_exit_AT_mode(serial)<0)
			DEBUG_RADIO_MSG("Could not Exit AT Mode");
	}
	else
	{
		DEBUG_RADIO_MSG("Could not Enter AT Mode");
		res = -1;
	}

	//Done
	return res;

}

uint8_t radio_send_raw_command(FILE *serial, uint8_t *data, uint8_t len)
{
	uint8_t res;
#if DEBUG_RADIO_RAW
	uint8_t i;

	printf("Radio Debug Message: SEND LEN %i", len);
	for(i=0;i<len;i++)
		printf("[0x%X]",data[i]);
	printf("\n");
#endif
	//Serial 8bit data is always 1byte to the radio
	res = fwrite(data,1,len,serial);
	fflush(serial);
	//If fwrite cannot write the whole packet to the radio
	//then we cannot continue
	if(res != len)
	{
		DEBUG_RADIO_MSG("Error Sending");
		return 0;
	}
	else
		return 1;
}

uint8_t radio_receive_raw_command(FILE *serial, uint8_t *data, uint8_t len)
{
	int fd, res;
	fd_set readfs;
	struct timeval Timeout;

	//Use Select() to stop blocking incase of timeouti
	fd = fileno(serial);
	Timeout.tv_usec = 0;
	Timeout.tv_sec = RADIO_REPLY_TIMEOUT;
	FD_SET(fd, &readfs);
	res = select(fd+1, &readfs, NULL, NULL, &Timeout);
	if(res == 0)
	{
		DEBUG_RADIO_MSG("Response Timeout");
		return 0;
	}
	else
	{
		if(FD_ISSET(fd, &readfs))
		{
			res = fread(data,1,len,serial);
		}
		else
		{
			DEBUG_RADIO_MSG("Receive ERROR Unknown");
			return 0;
		}
	}
#if DEBUG_RADIO_RAW
	//Print the recieved data out
	uint8_t i;
	printf("Radio Debug Message: RECV ");
	for(i=0;i<len;i++)
		printf("[0x%X]",(uint8_t)data[i]);
	printf("\n");
#endif
	if(res != len)
	{
		DEBUG_RADIO_MSG("Error Reading Data");
		return 0;
	}
	else
		return 1;
}

int radio_enter_sleep(FILE *serial, uint8_t *channel)
{
	uint8_t send[] = ENTER_SLEEP;
	uint8_t expectedRecv[] = ENTER_SLEEP_RESPONSE;
	uint8_t recv[sizeof(expectedRecv)];
	int res;

	DEBUG_RADIO_MSG("ENTERING SLEEP");
	//Enter AT Mode
	if(radio_enter_AT_mode(serial) > 0)
	{
		if(radio_send_raw_command(serial,send,sizeof(send)))
		{
			DEBUG_RADIO_MSG("Enter Sleep Command OK");
			if(radio_receive_raw_command(serial,recv,sizeof(recv)))
			{
				DEBUG_RADIO_MSG("Response OK");
				*channel = recv[1];
				res = 1;
			}
			else
				res = -1;
		}
		else
			res = -1;

		//Exit AT mode
		//if(radio_exit_AT_mode(serial)<0)
		//	DEBUG_RADIO_MSG("Could not Exit AT Mode");
		//DO NOT EXIT AT MODE
	}
	else
	{
		DEBUG_RADIO_MSG("Could not Enter AT Mode");
		res = -1;
	}

	//Done
	return res;
}

int radio_exit_sleep(FILE *serial, uint8_t *channel)
{
	uint8_t send[] = EXIT_SLEEP;
	uint8_t expectedRecv[] = EXIT_SLEEP_RESPONSE;
	uint8_t recv[sizeof(expectedRecv)];
	int res;

	//Cannot Enter AT mode because it was not exited previously
	DEBUG_RADIO_MSG("EXITING SLEEP");
	if(radio_send_raw_command(serial,send,sizeof(send)))
	{
		DEBUG_RADIO_MSG("Exit Sleep Command OK");
		if(radio_receive_raw_command(serial,recv,sizeof(recv)))
		{
			DEBUG_RADIO_MSG("Response OK");
			*channel = recv[1];
			res = 1;
		}
		else
			res = -1;
	}
	else
		res = -1;

	//Exit AT mode
	if(radio_exit_AT_mode(serial)<0)
		DEBUG_RADIO_MSG("Could not Exit AT Mode");

	//Done
	return res;
}

uint8_t radio_configure_from_file(FILE *serial, char *filename)
{
	FILE *inFile;
	char buff[RADIO_CONFIG_MAX_LINE_LENGTH];
	uint8_t i, j;
	uint8_t res;
	char c;
	unsigned int anInt1;
	unsigned int anInt2;
	unsigned int anInt3;

	char modeString[] = SET_MODE;
	char modeStringResponse[] = SET_MODE_RESPONSE;

	char destString[] = SET_DEST_ADDRESS;
	char destStringResponse[] = SET_DEST_ADDRESS_RESPONSE;

	char autoDestString[] = SET_AUTO_DEST_ADDRESS;
	char autoDestStringResponse[] = SET_AUTO_DEST_ADDRESS_RESPONSE;

	char powerString[] = SET_RADIO_POWER;
	char powerStringResponse[] = SET_RADIO_POWER_RESPONSE;

	DEBUG_RADIO_MSG("CONFIGURING RADIO FROM FILE");

	//open the config file
	inFile = fopen(filename, "r");
	//Check the file was opened correctly
	if(inFile == NULL)
	{
		DEBUG_RADIO_MSG("Error Opening File");
		return FALSE;
	}

	res = TRUE;
	//First Enter Configuration Mode
	if(radio_enter_AT_mode(serial) > 0)
	{
		//Perform configuration steps for each line
		for(i=0;i<RADIO_CONFIG_NUM_LINES;i++)
		{
			if(res == FALSE)
			{
#if DEBUG_RADIO
				printf("Error Configuring Radio at Line %d\n",i);
#endif
				break;
			}

			fgets(buff,RADIO_CONFIG_MAX_LINE_LENGTH,inFile);
			switch(i)
			{
				case RADIO_CONFIG_LINE_MODE:
					//Read in from the config file
					j = sscanf(buff,RADIO_CONFIG_LINE0_FORMAT_STRING,&c);
					if(c == 'C')
					{
						DEBUG_RADIO_MSG("Configuring as Client");
						modeString[2]=0x01;
						modeStringResponse[2]=0x01;
					}
					else if(c == 'S')
					{
						DEBUG_RADIO_MSG("Configuring as Server");
						modeString[2]=0x02;
						modeStringResponse[2]=0x02;
					}
					else
					{
						printf("Invalid mode (0x%x, scanf returns %d).\n", c, j);
					}
					//Send the command
					if(radio_send_raw_command(serial,modeString,sizeof(modeString)))
					{
						if(radio_receive_raw_command(serial, modeStringResponse, sizeof(modeStringResponse)))
							res = TRUE;
						else
						{
							DEBUG_RADIO_MSG("Set mode RECV Fail");
							res = FALSE;
						}
					}
					else
					{
						DEBUG_RADIO_MSG("Set mode SEND Fail");
						res = FALSE;
					}
					break;
				case RADIO_CONFIG_LINE_DEST:break;
					DEBUG_RADIO_MSG("Configuring Destination Address");
					//Read in from config file
					sscanf(buff,RADIO_CONFIG_LINE1_FORMAT_STRING,&anInt1,&anInt2,&anInt3);
					destString[2]=anInt1;	destString[3]=anInt2;	destString[4]=anInt3;
					destStringResponse[1]=anInt1;	destStringResponse[2]=anInt2;	destStringResponse[3]=anInt3;

					//Send the command
					if(radio_send_raw_command(serial,destString,sizeof(destString)))
					{
						if(radio_receive_raw_command(serial, destStringResponse, sizeof(destStringResponse)))
						{
							//Check the radio echoed back the correct cmd
							if((destString[2] != destStringResponse[1]) || (destString[3] != destStringResponse[2]) || (destString[4] != destStringResponse[3]) )
							{
								res = FALSE;
								DEBUG_RADIO_MSG("Set dest incorrect echo");
							}
							else
								res = TRUE;
						}
						else
						{
							DEBUG_RADIO_MSG("Set dest RECV Fail");
							res = FALSE;
						}
					}
					else
					{
						DEBUG_RADIO_MSG("Set dest SEND Fail");
						res = FALSE;
					}
					break;
				case RADIO_CONFIG_LINE_POWER:
					DEBUG_RADIO_MSG("Configuring Max Power");
					//Read in from Config file;
					sscanf(buff,RADIO_CONFIG_LINE2_FORMAT_STRING,&anInt1);
					powerString[2]=anInt1;
					powerStringResponse[1]=anInt1;
					//Send the command
					if(radio_send_raw_command(serial,powerString,sizeof(powerString)))
					{
						if(radio_receive_raw_command(serial, powerStringResponse, sizeof(powerStringResponse)))
						{
							//Check the radio echoed back the correct cmd
							if(powerString[2] != powerStringResponse[1])
							{
								res = FALSE;
								DEBUG_RADIO_MSG("Set power incorrect echo");
							}
							else
								res = TRUE;
						}
						else
						{
							DEBUG_RADIO_MSG("Set power RECV Fail");
							res = FALSE;
						}
					}
					else
					{
						DEBUG_RADIO_MSG("Set powers SEND Fail");
						res = FALSE;
					}
					break;
			}
		}
		DEBUG_RADIO_MSG("Configuring Auto Destination Address");
		autoDestString[2] = 0x11;
		//Send the command
		if(radio_send_raw_command(serial,autoDestString,sizeof(autoDestString)))
		{
			if(radio_receive_raw_command(serial, autoDestStringResponse, sizeof(autoDestStringResponse)))
			{
				//Check the radio echoed back the correct cmd
				if(autoDestStringResponse[1] != 1)
				{
					res = FALSE;
					DEBUG_RADIO_MSG("Set auto dest incorrect echo ");
					printf("%i", autoDestStringResponse[1]);
				}
				else
					res = TRUE;
			}
			else
			{
				DEBUG_RADIO_MSG("Set auto dest RECV Fail");
				res = FALSE;
			}
		}
		else
		{
			DEBUG_RADIO_MSG("Set auto dest SEND Fail");
			res = FALSE;
		}

	}
	else
	{
		DEBUG_RADIO_MSG("Could not enter AT mode");
		return FALSE;
	}

	if(radio_exit_AT_mode(serial))
	{
		DEBUG_RADIO_MSG("Exited AT mode");
		return TRUE;
	}
	else
	{
		DEBUG_RADIO_MSG("Could not Exit AT mode");
		return FALSE;
	}
}

int8_t radio_get_dest(FILE *serial, uint8_t *dest1, uint8_t *dest2, uint8_t *dest3)
{
	uint8_t res;
	char destString[] = GET_DEST_ADDRESS;
	char destStringResponse[] = GET_DEST_ADDRESS_RESPONSE;


	if(radio_enter_AT_mode(serial) > 0)
	{
		//Send the command
		if(radio_send_raw_command(serial,destString,sizeof(destString)))
		{
			if(radio_receive_raw_command(serial, destStringResponse, sizeof(destStringResponse)))
			{
				//Check the radio echoed back the correct cmd
				*dest1 = destStringResponse[1];
				*dest2 = destStringResponse[2];
				*dest3 = destStringResponse[3];
				res = 1;
			}
			else
			{
				DEBUG_RADIO_MSG("Get dest RECV Fail");
				res = -1;
			}
		}
		else
		{
			DEBUG_RADIO_MSG("Get dest SEND Fail");
			res = -1;
		}
		//Exit AT mode
		if(radio_exit_AT_mode(serial)<0)
			DEBUG_RADIO_MSG("Could not Exit AT Mode");
	}
	else
	{
		DEBUG_RADIO_MSG("Could not Enter AT Mode");
		res = -1;
	}

	return res;
}
