#ifndef _ALBATROSS_STD_H_
#define _ALBATROSS_STD_H_
#include <inttypes.h> //For uintxx_t definitions

#ifndef _CPP
typedef enum {FALSE=0, TRUE=1} bool_t;
#endif

//For extracting from 16 bit nums
#define LO_BYTE(X) ((uint8_t)(0x00FF & X))
#define HI_BYTE(X) ((uint8_t)((0xFF00 & X)>>8))
#define LONG_HI_HI_BYTE(X) ((uint8_t)((0xFF000000 & X)>>24))
#define LONG_HI_BYTE(X)    ((uint8_t)((0x00FF0000 & X)>>16))
#define LONG_LO_BYTE(X)    ((uint8_t)((0x0000FF00 & X)>>8))
#define LONG_LO_LO_BYTE(X) ((uint8_t)( 0x000000FF & X))


#ifdef _CPP
extern "C"
{
#endif

typedef enum {ALBA_INFO, ALBA_WARNING, ALBA_ERROR} alba_error_t;
void albatross_error(char *msg, uint16_t sender, alba_error_t type);

#ifdef _CPP
}
#endif


#endif
