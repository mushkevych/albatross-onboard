DESCRIPTION = "libscl compilation recipe"

PR = "r0"

DEPENDS = ""

SRC_URI = " \
	file://SCL.h		\
	file://SCL_config.h	\
	file://SCL_hash.h	\
	file://SCL_list.h	\
	file://SCL_map.h	\
	file://SCL_queuebuf.h	\
	file://SCL_stackbuf.h	\
	file://SCL_symbol.h	\
	file://SCL_tree.h	\
	file://SCL_vector.h 	\
	file://make_config.h	\
	file://config.c		\
	file://hash.c		\
	file://list.c		\
	file://map.c		\
	file://queuebuf.c	\
	file://stackbuf.c	\
	file://symbol.c		\
	file://tree.c		\
	file://vector.c 	\
	file://Makefile 	\
"

S = "${WORKDIR}"

do_compile () {
    oe_runmake libs
}

do_stage () {
        oe_libinstall -a -so libscl ${STAGING_LIBDIR}
        install -m 0644 SCL_map.h ${STAGING_INCDIR}
        install -m 0644 SCL.h ${STAGING_INCDIR}
}

do_install () {
    oe_libinstall -so -a libscl ${D}${libdir} 
    install -m 0644 ${S}/libscl.a ${D}${libdir}
#    install -m 0644 ${S}/libscl.so ${D}${libdir}
}

FILES_${PN} += "${libdir}/libscl.a"
#FILES_${PN} += "${libdir}/libscl.a ${libdir}/libscl.so"


