'''
Unit test to verify "aviation formulary"

Created on Apr 26, 2010
@author: Bohdan Mushkevych
'''
import unittest
from aviation_formulary import calculate_course_and_distance
from aviation_formulary import calculate_next_position
from aviation_formulary import ELLIPSE_A

class TestFormulary(unittest.TestCase):
    '''
    Unit tests covers several test cases. Among them - calculation of route between LA and NY:
    http://williams.best.vwh.net/avform.htm#Example
    
    point 1 is LAX: (33deg 57min N, 118deg 24min W)
    point 2 is JFK: (40deg 38min N,  73deg 47min W)
    
    position in radians:
    point 1 (LAX): lat1 = (33+57/60)*pi/180 = 0.592539; lon1 = (118+24/60)*pi/180 = 2.066470
    point 2 (JFK): lat2 = (40+38/60)*pi/180 = 0.709186; lon2 = (73+47/60)*pi/180 = 1.287762
    
    position in degrees: 
    point 1 (LAX): lat1 = 33 + 57/60 = 33.95;     lon1 = 118 + 24/60 = 118.4
    point 2 (JFK): lat2 = 40 + 38/60 = 40.633333; lon2 = 73 + 47/60 = 73.783333
    '''

    def test_course_and_distance(self):
        '''
        test calculates distance between two points

        pre-calculated values are:
        distance = 2144nm = 0.623585 radians = ELLIPSE_A * 0.623585 = xxx meters
        course = 1.150035 radians = 66 degrees     
        '''
        known_course = 66 
        known_distance = ELLIPSE_A * 0.623585

        latitude_1 = 33.95
        longtitude_1 = 118.4
        latitude_2 = 40.633333
        longtitude_2 = 73.783333

        tc, distance = calculate_course_and_distance(latitude_1, longtitude_1, latitude_2, longtitude_2)
        self.assertEqual(known_distance, distance)
        self.assertEqual(known_course, tc)

    def test_next_position_positive(self):
        '''
        test calculates latitude and longtitude of the point located in distance of 100nm over cource of 66 degrees
        
        pre-calculated values are:
        100nm = 100*pi/(180*60)=0.0290888radians
        lat = 0.604180 radians = 34degrees 37min N
        lon = 2.034206 radians = 116 degrees 33min W 
        '''
        
        known_latitude = 34.616667
        known_longtitude = 116.55
        distance_meters = ELLIPSE_A * 0.0290888
        
        latitude_1 = 33.95
        longtitude_1 = 118.4
        angle_degrees = 66

        latitude_2, longtitude_2 = calculate_next_position(latitude_1, longtitude_1, angle_degrees, distance_meters)
        self.assertEqual(known_latitude, latitude_2) 
        self.assertEqual(known_longtitude, longtitude_2) 
    
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
