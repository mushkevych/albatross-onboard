"""Pyrex wrapper for udp_multicast.h functions from onboard/lib
Use the default IP and port (can't be changed without editing the
Pyrex source). Default Rx buffer is 1kB. 

Hugo Vincent, 22 June 2005."""

#------------------------------------------------------------------------------
# Includes
include "common_cdef.pxd"
include "common_constants.pyx"

#------------------------------------------------------------------------------
# Functions in udp_multicast.h
cdef extern int multicast_recv(char *addr, int port, char *data, char *fromAddr)
cdef extern int multicast_send(char *addr, int port, char *data, int len)

#------------------------------------------------------------------------------

class IpcException(Exception):
	pass

# Function wrappers (allocate memory, do conversions etc.)
def recv():
	"""recv() -> Str
	Wraps C function multicast_recv, returns data as a Python string.
	Uses the DEFAULT_IP and DEFAULT_PORT only."""

	cdef char *data_buf, *from_buf
	cdef int len

	# Allocate the buffers 
	data_buf = <char *> malloc(sizeof(char) * BUF_SIZE)
	from_buf = <char *> malloc(sizeof(char) * UDP_FROM_STRING)
	if data_buf == NULL or from_buf == NULL:
		raise MemoryError

	# Receive
	len = multicast_recv(DEFAULT_IP, DEFAULT_PORT, data_buf, from_buf)
	if len < 0:
		raise IpcException

	# Convert data to a Python object
	ret_data = PyString_FromStringAndSize(data_buf, len)	

	# Free memory and return
	free(data_buf)
	free(from_buf)
	return ret_data

def send(data):
	"""send(Str data) -> Int
	Wraps C function multicast_send, returns number of bytes sent.
	Uses the DEFAULT_IP and DEFAULT_PORT only."""
	cdef int retval
	
	# Check its not going to overflow the Rx buffer
	if len(data) > BUF_SIZE:
		raise IpcException

	# Send it
	retval = multicast_send(DEFAULT_IP, DEFAULT_PORT, data, len(data))

	# Check for errors
	if retval < 0:
		raise IpcException
	return retval


