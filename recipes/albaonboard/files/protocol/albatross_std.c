#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>

#include "albatross_std.h"
#include "datagram.h"
#include "udp_multicast.h"
#include "protocol.h"

void albatross_error(char *msg, uint16_t sender, alba_error_t type)
{
	char logText[MAX_MESSAGE_LENGTH + 100];
	// Generate the error message
	if (type == ALBA_INFO) {
		strcpy(logText, "Info: ");
	}
	else if (type == ALBA_WARNING) {
		strcpy(logText, "Warning: ");
	}
	else if (type == ALBA_ERROR) {
		strcpy(logText, "Error: ");
	}
	strcat(logText, msg);
	sprintf(logText + strlen(logText), "(Sender = 0x%04x)\n", sender);

	// Write to an error log file
	FILE *errorLog;
	errorLog = fopen(ERROR_LOG_FILE, "a");
	if (errorLog == NULL) {
		fprintf(stderr,"ErrorLog: unable to open log file.\n");
		return; // Can't do anything without the file.
	}
	fprintf(errorLog, logText); // Write the item
	fprintf(stderr, logText); // Write the item
	fclose(errorLog);

	// Send a datagram as well
	datagram_send_log_message(logText, sender, 0);

	fprintf(stderr, logText); fflush(stderr);
}

