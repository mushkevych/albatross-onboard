#define FPGA_ADC0	fpga[4]
#define FPGA_ADC1	fpga[5]
#define FPGA_ADC2	fpga[6]
#define FPGA_ADC3	fpga[7]
#define FPGA_ADC4	fpga[8]
#define FPGA_ADC5	fpga[9]
#define FPGA_ADC6	fpga[10]
#define FPGA_ADC7	fpga[11]
#define FPGA_ADC_BITMASK 0x00000FFF

/*
  LLH stands for lat, long, height (standard WGS84)
  ECEF stands for Earth Centred Earth Fixed
  http://en.wikipedia.org/wiki/Geodetic_system
*/
#define SEND_LLH 1
#define SEND_ECEF 1
