/*------- Albatross UAV - Hardware Interface Daemon -------------------------*
 *
 *	File: commd.c
 *	Authors: John Stowers, Hugo Vincent
 *	Description: Configures the AAC4490 radio, then performs two tasks
 *	1) Forwards any datagrams with COMM_D as their destination
 *	over the radio to the other end.
 *	2) Reconstructs packets that arrive over the serial, and sends them
 *	back out as a datagram.
 *
 *
 *	Copyright (C) 2005  John Stowers, Hugo Vincent
 *	The code was from Albatross (http://johnstowers.no-ip.com).
 *	Distributed under GPL.
 */
/*-------Includes------------------------------------------------------------*/
#include <sys/signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "radio.h"
#include "commd.h"
#include "../protocol/albatross_std.h"
#include "../protocol/udp_multicast.h"
#include "../protocol/datagram.h"
#include "../protocol/protocol.h"
#include "../protocol/serial.h"
/*-------Constants-----------------------------------------------------------*/
/*-------Macros--------------------------------------------------------------*/
void serial_data_received_sig_handler(int status);
void datagram_callback(int status, void *user_data);
void transmit_datagram_to_udp(datagrizzle_t *datagram);
void transmit_datagram_to_serial(datagrizzle_t *datagram);
uint8_t non_block_select_read(int fd, int utimeout);

/*-------Globals-------------------------------------------------------------*/
//These live out here because it is used in the threaded serial reciever
FILE *serial;
bool_t radioAsleep = FALSE;

//Global signal flags
bool_t toSendFlag;
bool_t toRecvFlag;

//Mutex to protect sending serial data
pthread_mutex_t toSendMutex;
bool_t running;

//Datagram for recieving from datagram
datagrizzle_t DgRecv;

static int port;

static char *device_type;
/*-------Main Function-------------------------------------------------------*/
int main(int argc, char *argv[]) {
	char *device = argc < 2 ? "/dev/ttyS0" : argv[1];
	fprintf(stdout, "Usage: device %s \n", device);

	//Check serial port type
	device_type = argc < 3 ? "radio" : argv[2];
	fprintf(stderr, "Usage: device type %s\n", device_type);

	//Check multicast port
	port = argc < 4 ? UDP_DEFAULT_PORT : atoi(argv[3]);
	fprintf(stderr, "Usage: port %i\n", port);

	//Configure and oppen the serial port
	//The readOnly version is for the serial reader one in the thread
	serial = fopen(argv[1], "w+");
	if (!serial) {
		fprintf(stderr, "Could not open %s\n", argv[1]);
		return 2;
	}

	speed_t baudrate = strcmp(device_type, "radio") == 0 ? RADIO_BAUDRATE
			: B38400;
	if (!serial_configure(serial, baudrate, RADIO_HW_FLOW_CONTROL)) {
		fprintf(stderr, "Could not configure %s\n", argv[1]);
		return 3;
	}

	if (strcmp(device_type, "radio") == 0) {

#if USING_RADIO_AT_COMMANDS
		DEBUG_COMMD_MSG("Using Radio");
		//Initialise the Radio
#if CONFIGURE_RADIO_FROM_FILE
		//Check radio settings file
		char* conf = argc < 5 ? "radio.air.conf" : argv[4];
		fprintf(stdout, "Usage: %s radio settings file\n", conf);

		if (radio_configure_from_file(serial, conf)) {
			DEBUG_COMMD_MSG("Radio Configured From File");
		} else {
			DEBUG_COMMD_MSG("Error configuring radio from file.");
		}
#else
		DEBUG_COMMD_MSG("Radio Not Configured From File");
#endif

		uint8_t status, temp, ver, rssi;
		//FIXME: What other pretty radio things must we do here
		if (radio_get_status(serial, &ver, &status) > 0)
			printf("STATUS = 0x%X\n", status);
		if (radio_get_last_rssi(serial, &rssi) > 0)
			printf("RSSI = 0x%X\n", rssi);
		if (radio_read_temperature(serial, &temp) > 0)
			printf("TEMP = 0x%X\n", temp);
#if RADIO_WAIT_FOR_PAIRED
		//Wait for the radios to find each other
		uint8_t stat;
		uint8_t paired = FALSE;
		printf("Waiting for radios to find each other\n");
		while (!paired) {
			if (radio_get_status(serial, &ver, &stat) > 0) {
				//Status - 0x02 or 0x03 while searching for other radio
				if ((stat == 0x00) || (stat == 0x01)) {
					paired = TRUE;
					printf("\n");
				} else {
					paired = FALSE;
					printf(".");
					sleep(1);
				}
			} else
				sleep(1);

		}
#endif
#if RADIO_GO_STRAIGHT_TO_SLEEP
		uint8_t chan;
		if(radio_enter_sleep(serial,&chan) > 0)
		printf("SLEEP = 0x%X\n",chan);
#endif

#if RADIO_GET_DEST_ADDR
		uint8_t gotdest[3];
		if (radio_get_dest(serial, &gotdest[0], &gotdest[1], &gotdest[2]) > 0)
			printf("Got dest address: 0x%x 0x%x 0x%x\n", gotdest[0],
					gotdest[1], gotdest[2]);
#endif
#else
		DEBUG_COMMD_MSG("Not Using Radio");
#endif
	}
	//Initialise the datagram code
	datagram_clear(&DgRecv);
	datagram_init(&DgRecv, DATAGRAM_MAX_PAYLOAD_SIZE);
	//Initialise the datagram callbacks
	datagram_set_receive_bitmask(&DgRecv, COMM_D_BITMASK);
	datagram_set_callback(&DgRecv, datagram_callback, NULL);

	//initialize udp port to monitor
	DgRecv.port = port;

	running = TRUE;
	toRecvFlag = FALSE;
	toSendFlag = FALSE;
	pthread_mutex_init(&toSendMutex, NULL);

	//Now attach the thread and start it
	pthread_t rcv;
	if(strcmp(device_type, "radio") == 0){
	pthread_create(&rcv, NULL, &datagram_threaded_reciever, (void *) &DgRecv);
	}
	
	//Now start the serial reciever thread
	//The serial reciever threads arg is a datagram recieved over the network which can be forwarded
	//Local variable buffer
	pthread_t serialRcv;
	pthread_create(&serialRcv, NULL, &serial_threaded_reciever,
			(void *) &DgRecv);
	DEBUG_COMMD_MSG("Serial Reciever Thread Started");

#if DEBUG_COMMD
	printf("CommD Debug Message: Forwarding Network --> Serial %s\n",
			((ENABLE_FORWARDING_FROM_NETWORK_TO_SERIAL == 1) ? "ENABLED"
					: "DISABLED"));
	printf("CommD Debug Message: Forwarding Serial --> Network %s\n",
			((ENABLE_FORWARDING_FROM_SERIAL_TO_NETWORK == 1) ? "ENABLED"
					: "DISABLED"));
#endif

	if (strcmp(device_type, "radio") == 0) {
		pthread_join(rcv, NULL);
	}
	pthread_join(serialRcv, NULL);

	fflush(serial);
	fclose(serial);
	datagram_send_log_message("CommD Stopped", COMM_D_BITMASK, port);
	return 0;
}

/*-------Implementation: Functions-------------------------------------------*/
//Threaded Reciever Function
void *serial_threaded_reciever(void *tsd) {
	bool_t somethingToRecv;
	uint8_t ch;
	uint8_t chCounter;
	uint8_t payloadCounter;
	uint8_t payloadSize;
	bool_t invalidPacket;
	bool_t packetComplete;
	enum DecoderState_t state;

	//Main loop sleep timer
	//FIXME: Should the main loop sleep timers be in protocol.h?
	//FIXME: For some reason pause doesnt notice the send SIGUSR1 signal so use timer instead
	struct timespec timer;
	timer.tv_sec = 0;
	timer.tv_nsec = 10000000; //10ms

	// The serial thread has a datagram which it sends
	// Datagram for recieving from serial
	datagrizzle_t DgSerialRecv;
	datagram_clear(&DgSerialRecv);
	datagram_init(&DgSerialRecv, DATAGRAM_MAX_PAYLOAD_SIZE);
	datagram_set_port(&DgSerialRecv, port);
	//Begin the state machine to reassemble the serial data into a datagram
	state = STATE_SYNC0;
	chCounter = 0;
	payloadCounter = 0;

	invalidPacket = FALSE;
	packetComplete = FALSE;

	while (running) {
		//Wait for a signal
		nanosleep(&timer, NULL);

		if (radioAsleep)
			continue;

		int c;
		int serialfd = fileno(serial);

		//Now recieve the whole packet
		somethingToRecv = non_block_select_read(serialfd, 0);
		while (somethingToRecv && (c = fread(&ch, 1, 1, serial)) && c != EOF) {
#if DEBUG_COMMD_RAW
			fprintf(stdout,"CommD Debug Message: %d:[0x%X]\n", chCounter, ch);
			fflush(stdout);
#endif

			switch (state) {
			//First wait for the syncbytes in the first 2 positions
			case STATE_SYNC0:
				//DEBUG_COMMD_MSG("Sync0");
				//Insert character to the header
				DgSerialRecv.payload.header[chCounter] = ch;

				if ((ch == DATAGRAM_SYNC0_BYTE)
						&& (chCounter == DATAGRAM_SYNC0)) {
					state = STATE_SYNC1;
				} else {
					invalidPacket = TRUE;
				}
				break;

			case STATE_SYNC1:
				//DEBUG_COMMD_MSG("Sync1");
				//Insert character to the header
				DgSerialRecv.payload.header[chCounter] = ch;

				if ((ch == DATAGRAM_SYNC1_BYTE)
						&& (chCounter == DATAGRAM_SYNC1)) {
					state = STATE_HEADER;
				} else {
					invalidPacket = TRUE;
				}
				break;

			case STATE_HEADER:
				//Insert character to the header
				DgSerialRecv.payload.header[chCounter] = ch;

				if (chCounter == DATAGRAM_HEADER_CHSUM) {
					//fprintf(stdout,"CommD Debug Message: Reached Header End");

					payloadSize = datagram_get_payload_size(&DgSerialRecv);
					//fprintf(stdout,"CommD Debug Message: Payload Size = [0x%X]\n", payloadSize);

					//Check that received packet has valid header CRC
					if (datagram_check_header(&DgSerialRecv) == FALSE) {
						invalidPacket = TRUE;
					}
					state = STATE_PAYLOAD;

					//LWC have no payload
					if (payloadSize != 0) {
						break;
					}
				} else
					break;

			case STATE_PAYLOAD:
				if (payloadSize == 0) {
					state = STATE_DATA_CHSUM;
				} else {
					//DEBUG_COMMD_MSG("Payload");
					//Insert the data into the datagram
					DgSerialRecv.payload.data[payloadCounter] = ch;
					payloadCounter++;
					if (payloadCounter == payloadSize) {
						state = STATE_DATA_CHSUM;
					} else
						break;
				}

			case STATE_DATA_CHSUM:
				if (datagram_check_fletcher(&DgSerialRecv)) {
					DEBUG_COMMD_MSG("Fletcher OK");
					transmit_datagram_to_udp(&DgSerialRecv);

					//Reset state machine, and prepare for new datagram from serial port
					packetComplete = TRUE;
				} else {
					//odemkovych
					//TODO: verify if set this flag is to true here is correct
					invalidPacket = TRUE;

					DEBUG_COMMD_MSG("Fletcher FAIL");
				}

				break;
			}

			//Post increment (after data has been stored because
			//we reuse the array (0 refed) indexed constants from datagram.h
			chCounter++;
			//Check for state overrun
			if ((chCounter == 0xFF) || (invalidPacket == TRUE)
					|| (packetComplete == TRUE)) {
				if ((chCounter == 0xFF) || (invalidPacket == TRUE)) {
					DEBUG_COMMD_MSG("ERROR: Invalid Serial Packet");
				} else {
					DEBUG_COMMD_MSG("SUCCESS: Recieved Serial Packet");
				}

				//Reset all the state vars
				state = STATE_SYNC0;
				chCounter = 0;
				payloadCounter = 0;

				invalidPacket = FALSE;
				packetComplete = FALSE;

				somethingToRecv = FALSE;
				toRecvFlag = FALSE;

				datagram_clear(&DgSerialRecv);

			}
		}//end reading

		if (toSendFlag == TRUE) {
			//So main cannot send any packets until this one has been sent
			pthread_mutex_lock(&toSendMutex);

			uint8_t sizeOfPayload = datagram_get_payload_size(&DgRecv);
			uint8_t sizeOfHeader = DATAGRAM_HEADER_LEN;
			radio_send_raw_command(serial, DgRecv.payload.header, sizeOfHeader);
			radio_send_raw_command(serial, DgRecv.payload.data, sizeOfPayload);
			toSendFlag = FALSE;

			//Finally unlock the mutex and we are done
			pthread_mutex_unlock(&toSendMutex);

		}//end sending

	}//thread while


	printf("Reciever Stopped - Exiting\n");
	return NULL;
}

// function is called to retransmit datagram, received from serial port to UPD port
// all verifications for data correctness must be performed prior to calling this function
void transmit_datagram_to_udp(datagrizzle_t *datagram) {
	uint16_t dest;
	uint8_t headerChecksum;
	uint16_t dataChecksum;

	DEBUG_COMMD_MSG("SENDING: Serial --> Network");

	//Send the datagram back over the network
	//But first remove commd from the destination
	//of the emmited packet or else it will loop packets for ever
#if ENABLE_FORWARDING_FROM_SERIAL_TO_NETWORK

	dest = datagram_get_destination(datagram);
	if(strcmp(device_type, "radio") == 0){
	dest &= ~COMM_D_BITMASK;
	}
	datagram_set_destination(datagram, dest);

	//Because we have changed the packet, we must re-generate the checksums
	headerChecksum = datagram_build_header_checksum(datagram);
	datagram_set_hchsum(datagram, headerChecksum);

	//Generate the complete checksum
	dataChecksum = datagram_build_fletcher_checksum(datagram);
	datagram_set_chsum(datagram, dataChecksum);

	//Now its ready to send
	datagram_send(datagram);
#endif

}

void serial_data_received_sig_handler(int status) {
	toRecvFlag = TRUE;
}

void datagram_callback(int cmd, void *user_data) {
	//printf("Callback = %#X\n",cmd);
	if (cmd != -1) {
		if (cmd == LWC_COMMD_EXIT) {
			running = FALSE;
			DgRecv.recieverStopped = TRUE;
		}
#if USING_RADIO_AT_COMMANDS
		// Process these commands only and don't forward them
		else if (cmd == LWC_RADIO_SLEEP) {
			uint8_t chan;
			//datagram_send_log_message("Sleeping Radio", COMM_D_BITMAKS);
			if (radio_enter_sleep(serial, &chan) > 0) {
				radioAsleep = TRUE;
				printf("SLEEP = 0x%X\n", chan);
			}
		} else if (cmd == LWC_RADIO_UNSLEEP) {
			uint8_t chan;
			//datagram_send_log_message("Unsleeping Radio", COMM_D_BITMASK);
			if (radio_exit_sleep(serial, &chan) > 0) {
				radioAsleep = FALSE;
				printf("SLEEP = 0x%X\n", chan);
			}
		}
#endif
		// Else forward everything else addressed to CommD
		else {
			DEBUG_COMMD_MSG("SENDING: Network --> Serial");
#if ENABLE_FORWARDING_FROM_NETWORK_TO_SERIAL
#if DEBUG_COMMD_RAW
			payload_print(&DgRecv.payload);
#endif
			//Will block here if there is already data being
			//forwarded
			pthread_mutex_lock(&toSendMutex);
			toSendFlag = TRUE;
			pthread_mutex_unlock(&toSendMutex);
#endif
		}
	}
}

uint8_t non_block_select_read(int fd, int utimeout) {
	fd_set readfs;
	struct timeval timeout;

	//watch fd
	FD_SET(fd,&readfs);
	//timeout - 0 aka poll
	timeout.tv_sec = 0;
	timeout.tv_usec = utimeout;
	//poll the port
	select(fd + 1, &readfs, NULL, NULL, &timeout);
	//return result
	return FD_ISSET(fd,&readfs);
}
