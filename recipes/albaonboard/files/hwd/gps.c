/*------- Albatross UAV - GPS Interface Functions ---------------------------*
 *	
 *	File: gps.c
 *	Authors: John Stowers, Hugo Vincent
 *	Description: Talks to the uBloxk GPS reciever over serial port and decodes
 *	uBlox binary data.
 *  
 *  Copyright (C) 2005  John Stowers, Hugo Vincent
 *	The code was from Albatross (http://johnstowers.no-ip.com).
 *	Distributed under GPL.
 */


/* Part of this file is:
 *		Copyright (C) 2003  Pascal Brisset, Antoine Drouin
 *		The code was from paparazzi (http://www.nongnu.org/paparazzi).
 *		Distributed under GPLv2.
 */


/*-------Includes------------------------------------------------------------*/
#include <stdio.h>
#include <inttypes.h>
#include <string.h> 
#include <math.h>
#include "gps.h"
#include "../protocol/albatross_std.h"
#include "../protocol/serial.h"

/*-------Constants-----------------------------------------------------------*/
#define UBX_MAX_PAYLOAD	255
enum statemachine {UNINIT = 0, GOT_SYNC1, GOT_SYNC2, GOT_CLASS, GOT_ID, 
	GOT_LEN1, GOT_LEN2, GOT_PAYLOAD, GOT_CHECKSUM1};
const int32_t utm_east0 = NAV_UTM_EAST0;
const int32_t utm_north0 = NAV_UTM_NORTH0;

/*-------Macros--------------------------------------------------------------*/
#define RadianToDeg(d)	(((d)*180.0)/3.1415927)

/*-------Globals-------------------------------------------------------------*/
static uint8_t  ubx_msg_buf[UBX_MAX_PAYLOAD];
static uint8_t  ubx_status;
static uint16_t ubx_len;
static uint8_t  ubx_msg_idx;
static uint8_t ck_a, ck_b, ubx_id, ubx_class;
static uint8_t gps_nb_ovrn;


/*-------Implementation: Functions-------------------------------------------*/
void parse_gps_msg(struct GPSData_t *g)
{
	if (ubx_class == UBX_NAV_ID)
	{
		//fprintf(stdout,"."); fflush(stdout);
		if (ubx_id == UBX_NAV_POSUTM_ID)
		{
			//fprintf(stdout,"U"); fflush(stdout);
			g->UTMEast = UBX_NAV_POSUTM_EAST(ubx_msg_buf);
			g->UTMNorth = UBX_NAV_POSUTM_NORTH(ubx_msg_buf);
			g->UTMAltitude = (float)(UBX_NAV_POSUTM_ALT(ubx_msg_buf)/100);
		} 
		else if (ubx_id == UBX_NAV_STATUS_ID)
		{
			//fprintf(stdout,"S"); fflush(stdout);
			g->mode = UBX_NAV_STATUS_GPSfix(ubx_msg_buf);
		}
		else if (ubx_id == UBX_NAV_POSLLH_ID)
		{
			//fprintf(stdout,"L"); fflush(stdout);
			g->latitude = UBX_NAV_POSLLH_LAT(ubx_msg_buf) / 1e7;
			g->longitude = UBX_NAV_POSLLH_LON(ubx_msg_buf) / 1e7;
			g->altitude = UBX_NAV_POSLLH_HEIGHT(ubx_msg_buf) / 1e3;
		}
		else if (ubx_id == UBX_NAV_SOL_ID)
		{
			//fprintf(stdout,"O"); fflush(stdout);
			g->satsInView = UBX_NAV_SOL_numSV(ubx_msg_buf);
			g->ECEFx = UBX_NAV_SOL_ECEF_X(ubx_msg_buf) / 100.0;
			g->ECEFy = UBX_NAV_SOL_ECEF_Y(ubx_msg_buf) / 100.0;
			g->ECEFz = UBX_NAV_SOL_ECEF_Z(ubx_msg_buf) / 100.0;
			g->ECEFvelx = UBX_NAV_SOL_ECEFVX(ubx_msg_buf) / 100.0;
			g->ECEFvely = UBX_NAV_SOL_ECEFVY(ubx_msg_buf) / 100.0;
			g->ECEFvelz = UBX_NAV_SOL_ECEFVZ(ubx_msg_buf) / 100.0;
		}
		else if (ubx_id == UBX_NAV_VELNED_ID)
		{
			//fprintf(stdout,"V"); fflush(stdout);
			g->gspeed = ((float)UBX_NAV_VELNED_GSpeed(ubx_msg_buf)) / 1e2; 
			g->climb = ((float)UBX_NAV_VELNED_VEL_D(ubx_msg_buf)) / -1e2;
			g->heading = ((float)UBX_NAV_VELNED_Heading(ubx_msg_buf)) * M_PI / (180.0 * 1e5);
			g->timeOfWeek = UBX_NAV_VELNED_ITOW(ubx_msg_buf);
			//gps_east = g->UTMEast / 100 - NAV_UTM_EAST0;
			//gps_north = g->UTMNorth / 100 - NAV_UTM_NORTH0;
		}
	}
}

void parse_ubx(struct GPSData_t *g, uint8_t c)
{
	if (ubx_status < GOT_PAYLOAD)
	{
		ck_a += c;
		ck_b += ck_a;
	}
	switch (ubx_status)
	{
		case UNINIT:
			if (c == UBX_SYNC1)
				ubx_status++;
			break;
		case GOT_SYNC1:
			if (c != UBX_SYNC2)
				goto error;
			ck_a = 0;
			ck_b = 0;
			ubx_status++;
			break;
		case GOT_SYNC2:
			if (g->msgReceived)
			{
				/* Previous message has not yet been parsed: discard this one */
				gps_nb_ovrn++;
				goto error;
			}
			ubx_class = c;
			ubx_status++;
			break;
		case GOT_CLASS:
			ubx_id = c;
			ubx_status++;
			break;    
		case GOT_ID:
			ubx_len = c;
			ubx_status++;
			break;
		case GOT_LEN1:
			ubx_len |= (c<<8);
			if (ubx_len > UBX_MAX_PAYLOAD)
				goto error;
			ubx_msg_idx = 0;
			ubx_status++;
			break;
		case GOT_LEN2:
			ubx_msg_buf[ubx_msg_idx] = c;
			ubx_msg_idx++;
			if (ubx_msg_idx >= ubx_len)
			{
				ubx_status++;
			}
			break;
		case GOT_PAYLOAD:
			if (c != ck_a)
				goto error;
			ubx_status++;
			break;
		case GOT_CHECKSUM1:
			//printf("got message\n");
			if (c != ck_b)
				goto error;
			g->msgReceived = TRUE;
			goto restart;
			break;
	}
	return;

	error:  
	restart:
		ubx_status = UNINIT;
	return;
}

void print_gps(struct GPSData_t *g)
{
	printf("gps_mode = %d\n", g->mode);
	printf("gps_itow = %u\n", g->timeOfWeek);
	printf("gps_gspeed = %f\n", g->gspeed);
	printf("gps_climb = %f\n", g->climb);
	printf("gps_heading = %f\n", g->heading);
	printf("gps_falt = %f\n", g->altitude);
	printf("gps_flatitude = %f\n", g->latitude);
	printf("gps_flongitude = %f\n", g->longitude);
	printf("gps_sats = %d\n", g->satsInView);
	
	printf("gps_ECEFx = %f\n", g->ECEFx);
	printf("gps_ECEFy = %f\n", g->ECEFy);
	printf("gps_ECEFz = %f\n", g->ECEFz);
	printf("gps_ECEFvelx = %f\n", g->ECEFvelx);
	printf("gps_ECEFvely = %f\n", g->ECEFvely);
	printf("gps_ECEFvelz = %f\n", g->ECEFvelz);
	
//	printf("gps_east = %f\n", gps_east);
//	printf("gps_north = %f\n", gps_north);
}

FILE *gps_init(char *serial_port)
{
	FILE *gps;
	uint8_t i;
	
	gps = fopen(serial_port,"a+");
	ubx_status = 0;
	
	if(gps != NULL)
	{
		//Thrid arg is false as GPS doesnt support HW flow control
		i = serial_configure(gps,GPS_BAUD_RATE,0);
		if(i) //Successful
		 	return gps;
		else
			return NULL;
	}
	else
		return NULL;
}
		

