'''
Module presents Route, as list of waypoints
Current logic is that waypoints have to be processed in order from the list 

@date: Apr 24, 2010
@author: Bohdan Mushkevych
'''

from collections import deque

class Route:

    def __init__(self):
        self.route = deque()

    def __len__(self):
        return len(self.route)
    
    def __getitem__(self, key):
        return self.route[key]
        
    def __setitem__(self, key, value):
        self.route[key] = value
        
    def __iter__(self):
        return iter(self.route)
    
    def __contains__(self, item):
        return item in self.route
    
    def __eq__(self, other):
        if not isinstance(other, Route) \
           or len (self.route) != len(other.route):
            return False

        for i in range(len(self.route)): 
            if self.route[i] != other.route[i]:
                return False
        
        return True
            
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def add(self, waypoint):
        self.route.append(waypoint)
    
    def remove(self, waypoint):
        self.route.remove(waypoint)
        
    def get_active(self):
        return self.route[0]
