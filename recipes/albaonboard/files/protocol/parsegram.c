#include <stdio.h>
#include <stdlib.h>

#include "parsegram.h"
#include "albatross_std.h"

// **************************** Parsegram Init functions *****************************
/**
 * functions creates new parsegram
 */
void parsegram_init(parsegram_t* p) 
{
   p->mapParsegram = map_new();
}

/**
 * functions clears the parsegram, by removing all entries
 */
void parsegram_clear(parsegram_t* p) 
{
  // clear all entries in the map
  map_erase(p->mapParsegram);
}

/**
 * functions deletes the parsegram, by freeing its memory
 */
void parsegram_delete(parsegram_t* p) 
{
  // deletes the map
  map_del(p->mapParsegram);
}

// **************************** Parsegram Access functions *****************************
/** 
 * function inserts elements into the parsegram
 * existing elements are overwritten
 * value is presented as array of chars (uint8_t)
 */
void parsegram_put(parsegram_t* p, uint8_t key, uint8_t *value) 
{
  map_replace (p->mapParsegram, key, value);
}

/** 
 * function removes element from parsegram
 * element is specified by its key
 */
void parsegram_remove(parsegram_t* p, uint8_t key) 
{
  map_remove(p->mapParsegram, key);
}

/** 
 * function returns element from parsegram
 * element is specified by its key
 * if no element with given key is found, NULL is returned
 */
uint8_t* parsegram_get(parsegram_t* p, uint8_t key) 
{
  void * data;
  SCL_iterator_t iterator;

  iterator = map_at (p->mapParsegram, key);
  if (iterator != NULL) {
    data = map_data_get (iterator);
  } else {
    data = NULL;
  }

  return data;
}

// **************************** Parsegram Conversions functions *****************************


/**
 * method inserts float value to the parsegram
 */
void parsegram_put_float(parsegram_t* p, uint8_t key, float value)
{
  uint32_t asFloat;
  uint8_t *array = calloc(4, sizeof(uint8_t));
  
  asFloat = *((uint32_t *)&value);
	array[0] = LONG_HI_HI_BYTE(asFloat);
	array[1] = LONG_HI_BYTE(asFloat);
	array[2] = LONG_LO_BYTE(asFloat);
	array[3] = LONG_LO_LO_BYTE(asFloat);
	
	parsegram_put(p, key, array); 
}

/**
 * TODO: work-around NULL pointer 
 *
 */
float parsegram_get_as_float(parsegram_t* p, uint8_t key)
{
  uint32_t asFloat;
  float aFloat;
  uint8_t* value = parsegram_get(p, key);

  aFloat = 0;
  asFloat = 0;	
  if (value != NULL) 
  {
    asFloat |= (0xFF000000 & (value[0] << 24));
    asFloat |= (0x00FF0000 & (value[1] << 16));
    asFloat |= (0x0000FF00 & (value[2] << 8));
    asFloat |= (0x000000FF & value[3]);

    aFloat = *((float *)&asFloat);
  }
  
  return aFloat;
}

/**
 * method inserts byte value to the parsegram
 */
void parsegram_put_byte(parsegram_t* p, uint8_t key, uint8_t value)
{
  uint8_t *array = calloc(1, sizeof(uint8_t));
  array[0] = value;
	parsegram_put(p, key, array); 
}

/**
 * TODO: work-around NULL pointer 
 *
 */
uint8_t parsegram_get_as_byte(parsegram_t* p, uint8_t key)
{
  uint8_t asByte;
  uint8_t* value = parsegram_get(p, key);

  asByte = 0;
  if (value != NULL) 
  {
    asByte = value[0];
  }
  
  return asByte;
}

/**
 * method inserts short value to the parsegram
 */
void parsegram_put_short(parsegram_t* p, uint8_t key, uint16_t value)
{
  uint8_t *array = calloc(2, sizeof(uint8_t));
  
	array[0] = HI_BYTE(value);
	array[1] = LO_BYTE(value);
	
	parsegram_put(p, key, array); 
}

/**
 * TODO: work-around NULL pointer 
 *
 */
uint16_t parsegram_get_as_short(parsegram_t* p, uint8_t key)
{
  uint16_t asShort;
  uint8_t* value = parsegram_get(p, key);

  asShort = 0;
  if (value != NULL) 
  {
    asShort |= (0xFF00 & (value[0] << 8));
    asShort |= (0x00FF & value[1]);
  }
  
  return asShort;
}

/**
 * method inserts integer value to the parsegram
 */
void parsegram_put_integer(parsegram_t* p, uint8_t key, uint32_t value)
{
  uint8_t *array = calloc(4, sizeof(uint8_t));
  
	array[0] = LONG_HI_HI_BYTE(value);
	array[1] = LONG_HI_BYTE(value);
	array[2] = LONG_LO_BYTE(value);
	array[3] = LONG_LO_LO_BYTE(value);
	
	parsegram_put(p, key, array); 
}

/**
 * TODO: work-around NULL pointer 
 *
 */
uint32_t parsegram_get_as_integer(parsegram_t* p, uint8_t key)
{
  uint32_t asInt;
  uint8_t* value = parsegram_get(p, key);

  asInt = 0;	
  if (value != NULL) 
  {
    asInt |= (0xFF000000 & (value[0] << 24));
    asInt |= (0x00FF0000 & (value[1] << 16));
    asInt |= (0x0000FF00 & (value[2] << 8));
    asInt |= (0x000000FF & value[3]);
  }
  
  return asInt;
}

/**
 * function return number of parsegram entries
 */
uint8_t parsegram_get_size(parsegram_t *p) 
{
  return map_count(p->mapParsegram);
}

/**
 * function return array of keys
 */
uint8_t* parsegram_get_keys(parsegram_t *p)
{
  uint8_t i;
  long* lKeys = map_get_keys(p->mapParsegram);
  uint8_t mapCount = map_count(p->mapParsegram);
  uint8_t *uKeys = calloc(mapCount, sizeof(uint8_t));

  for (i = 0; i < mapCount; i++) 
  {
    uKeys[i] = (uint8_t) lKeys[i];
  }
  
  // we have to take care of Keys memory
  free(lKeys);
  
  return uKeys;
}
  
