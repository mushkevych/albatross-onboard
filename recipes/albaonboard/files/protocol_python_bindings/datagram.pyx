"""Pyrex wrapper for datagram.h functions from onboard/lib
Provides the Datagram class that encapsulates the C API.
Uses the default IP, port and buffer size.

Example: SENDING
  import datagram
  d = datagram.Datagram()
  d.set_destination(0xFFFF)
  d.set_command(datagram.LWC_DATA)
  d.add_float(datagram.STATE_D_VELOCITY_AIRSPEED, 1.234)
  d.pack_and_send()
  d.display()

Example: RECIEVING (THREADED)
  import datagram, parsegram, time
  d = datagram.Datagram()
  d.set_start_addr(datagram.STATE_D_ADDR_START)
  d.set_end_addr(datagram.STATE_D_ADDR_END)
  d.start_threaded_recv()
  while True:
    if d.received_and_begin_processing():
      cmd = d.process(0xFFFF) # Listen for anything
      if cmd == 1:
      pg = r.getParsegram()
      for address in state_addresses:
        f= pg.get_float(address)
        print 'address = %', address, ' float= %', f
    d.end_processing()
    time.sleep(0.01) # 100 Hz

Hugo Vincent, 22 June 2005.
Bohdan Mushkevych, 2009"""

#------------------------------------------------------------------------------
# Include

include "common_constants.pyx"

#------------------------------------------------------------------------------
# A generic datagram exception
class IpcException(Exception):
  pass

# Static methods
def send_log_message(msg, sender):
  """Send a log message to the LogD."""
  datagram_send_log_message(msg, sender, 0)

#------------------------------------------------------------------------------
# The class Datagram (Pythonized API)
cdef class Datagram:

  def __cinit__ (self):
    """Allocate and initialize the C structure (datagram_t) and
    run datagram_clear and datagram_init on it."""
    
    # Allocate/init the C structure
    self.dg = <datagrizzle_t*>malloc(sizeof(datagrizzle_t))
    if self.dg == NULL:
      raise MemoryError
    datagram_clear(self.dg)
    datagram_init(self.dg, MAX_PAYLOAD_SIZE)

    self.start_addr = -1
    self.end_addr = -1

    self.sleepTime = 200

  def __dealloc__(self):
    free(self.dg)

  def set_destination(self, dest): 
    """Set the destination field bitmask."""
    datagram_set_destination(self.dg, dest)

  def get_destination(self):
    """Return the destination field bitmask."""
    return datagram_get_destination(self.dg)

  def set_sender(self, sender):
    """Set the sender field bitmask.""" 
    datagram_set_sender(self.dg, sender)

  def get_sender(self):
    """Return the sender bitmask."""
    return datagram_get_sender(self.dg)

  def set_command(self, cmd): 
    """Set the Light Weight Command field."""
    datagram_set_command(self.dg, cmd)

  def get_command(self):
    """Return the LWC field."""
    return datagram_get_command(self.dg)

  def add_float(self, addr, data):
    """Add a float value to the datagram at specified address.""" 
    datagram_add_float(self.dg, addr, data)

  def add_byte(self, addr, data):
    """Add a byte to the datagram at the specified address."""
    datagram_add_byte(self.dg, addr, data)

  def add_short(self, addr, data):
    """Add a short to the datagram at the specified address."""
    datagram_add_short(self.dg, addr, data)

  def add_string(self, addr, data):
    """Add a string to the datagram at the specified address."""
    datagram_add_string(self.dg, addr, data)

  def add_int(self, addr, data):
    """Add a (32-bit) integer to the datagram at the specified address."""
    datagram_add_int(self.dg, addr, data)


  def process(self, bitmask):
    """Process a received datagram, extracting all the variables that
    are in this daemons address space (set with set_start_addr(),
    set_end_addr()). 
    Data can subsequently be extracted from the parsegram with get_float() et al."""
    cdef int16_t retval
    retval = datagram_process(self.dg, bitmask)
    if retval == -1:
      raise IpcException("datagram_process() returned error.")
    if self.start_addr == -1 or self.end_addr == -1:
      raise IpcException("Set the address space before calling process().")

    self.parsegram_object = parsegram.Parsegram()  # the Pyrex parsegram structure
    datagram_process_payload(<parsegram_t *>self.parsegram_object.pg, self.dg, self.start_addr, self.end_addr)
    return retval

  def received_and_begin_processing(self):
    """Checks if the receiver thread has received a datagram; 
    if so, returns True, else returns False."""
    cdef uint8_t i
    i = datagram_recieved_and_begin_processing(self.dg)
    if i == 1:  
      return True
    else:
      return False


  def getParsegram(self):
    """ returns current parsegram. new Parsegram is created for every 'process' call"""
    return self.parsegram_object

  def end_processing(self):
    """Signal to the underlying C API that we have finished processing 
    this datagram."""
    datagram_end_processing(self.dg)

  def set_start_addr(self, addr):
    """Set the start address for the local address space for this daemon."""
    if addr > 0xFFFF:
      raise IpcException
    else:
      self.start_addr = addr

  def set_port(self, port):
    """Set udp port to send data to"""
    datagram_set_port(self.dg, port)

  def set_end_addr(self, addr):
    """Set the end address for the local address space for this daemon."""
    if addr < 0:
      raise IpcException
    else:
      self.end_addr = addr

  def pack_and_send(self):
    """Pack the datagram (including adding the checksums etc.) and
    send the datagram by multicast."""
    datagram_pack(self.dg)
    datagram_send(self.dg)

  def display(self): 
    """Print the datagram out in a human-readable form."""
    datagram_print(self.dg)

  def clear(self):
    """Clear the datagram so it can be reused."""
    datagram_clear(self.dg)

  def start_threaded_recv(self):
    """Start a separate thread that listens for incoming datagrams."""
    cdef pthread_t rcv
    pthread_create(&rcv, NULL, &datagram_threaded_reciever, self.dg)
    self.dg.recieverStopped = 0

  def stop_threaded_recv(self):
    """Stop a receiver thread started with start_threaded_recv()."""
    self.dg.recieverStopped = 1


