DESCRIPTION = "Albatross V2 Onboard Compilation Recipe"

PR = "r0"

DEPENDS = "gpsd libscl python python-pyrex"

SRC_URI = " \
	file://Makefile \
	file://enable_multicast_onboard.sh \
	file://autopilot/autopilot.py \
	file://autopilot/aviation_formulary.py \
	file://autopilot/__init__.py \
	file://autopilot/simple_autopilot_daemon.py \
	file://autopilot/Makefile \
	file://autopilot/servo.py \
	file://autopilot/route.py \
	file://autopilot/test_autopilot.py \
	file://autopilot/test_aviation_formulary.py \
	file://autopilot/test_route.py \
	file://autopilot/test_route_transmission.py \
	file://autopilot/waypoint.py \
	file://protocol/albatross_std.h \
	file://protocol/datagram.h \
	file://protocol/parsegram.h \
	file://protocol/protocol.h \
	file://protocol/serial.h \
	file://protocol/udp_multicast.h \
	file://protocol/albatross_std.c \
	file://protocol/datagram.c \
	file://protocol/Makefile \
	file://protocol/parsegram.c \
	file://protocol/serial.c \
	file://protocol/udp_multicast.c \
	file://protocol_python_bindings/common_cdef.pxd \
	file://protocol_python_bindings/common_constants.pyx \
	file://protocol_python_bindings/datagram.pxd \
	file://protocol_python_bindings/datagram.pyx \
	file://protocol_python_bindings/Makefile \
	file://protocol_python_bindings/parsegram.pxd \
	file://protocol_python_bindings/parsegram.pyx \
	file://protocol_python_bindings/protocol.pyx \
	file://protocol_python_bindings/udp_multicast.pyx \
	file://commd/commd.c \
	file://commd/commd.h \
	file://commd/radio.air.conf \
	file://commd/radio.c \
	file://commd/radio.ground.conf \
	file://commd/radio.h \
	file://commd/serialrecv.c \
	file://commd/Makefile \
	file://logd/logd.c \
	file://logd/logd.h \
	file://logd/Makefile \
	file://gps/alba_gpsd_client.c \
	file://gps/Makefile \
	file://supervisord/supervisord.c \
	file://supervisord/processes.conf \
	file://supervisord/Makefile \
"

S = "${WORKDIR}"
ALBA_DIR = "${prefix}/local/albatross"
AUTOPILOT_DIR = "${ALBA_DIR}/autopilot"

do_compile () {
    oe_runmake STAGING_LIBDIR=${STAGING_LIBDIR} STAGING_INCDIR=${STAGING_INCDIR}
}

do_install () {
		install -m 0755 -d ${D}${ALBA_DIR}
		install -m 0755 -d ${D}${AUTOPILOT_DIR}
		install -m 0755 ${S}/autopilot/*.py ${D}${AUTOPILOT_DIR}
		install -m 0755 ${S}/autopilot/*.so ${D}${AUTOPILOT_DIR}
		install -m 0755 ${S}/commd/obj-arm/commd ${D}${ALBA_DIR}
		install -m 0644 ${S}/commd/radio.air.conf ${D}${ALBA_DIR}
#		install -m 0755 ${S}/hwd/obj-arm/hwd ${D}${ALBA_DIR}
		install -m 0755 ${S}/gps/obj-arm/alba_gpsd_client ${D}${ALBA_DIR}
		install -m 0755 ${S}/logd/obj-arm/logd ${D}${ALBA_DIR}
		install -m 0755 ${S}/supervisord/obj-arm/supervisord ${D}${ALBA_DIR}
		install -m 0755 ${S}/supervisord/processes.conf ${D}${ALBA_DIR}
		install -m 0755 ${S}/enable_multicast_onboard.sh ${D}${ALBA_DIR}
}

FILES_${PN} += "${AUTOPILOT_DIR}/*.py ${AUTOPILOT_DIR}/*.so ${ALBA_DIR}/alba_gpsd_client ${ALBA_DIR}/commd ${ALBA_DIR}/radio.air.conf ${ALBA_DIR}/logd ${ALBA_DIR}/supervisord ${ALBA_DIR}/processes.conf ${ALBA_DIR}/enable_multicast_onboard.sh"

