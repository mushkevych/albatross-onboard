#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "radio.h"
#include "../include/albatross_std.h"
#include "../include/udp_multicast.h"
#include "../include/datagram.h"
#include "../include/protocol.h"
#include "../include/serial.h"

int main (int argc, char *argv[])
{
	//Serial port and radio stuff	
	FILE *serial;	
	uint8_t ch;
	
	//Check the command line stuff
	if (argc != 2)
	{
		fprintf(stderr, "Usage: commd /dev/ttyS0\n");
		return 1;
	}
	//Configure and oppen the serial port
	serial = fopen (argv[1], "rw+");
	if (!serial)
	{
		fprintf (stderr, "Could not open %s\n.", argv[1]);
		return 2;
	}

	if (!serial_configure(serial, RADIO_BAUDRATE))
	{
		fprintf (stderr, "Could not configure %s\n.", argv[1]);
		return 3;
	}

	while(1)
	{
		ch = fgetc(serial);
		fprintf(stdout,"[0x%X]",ch);
	}

}

