'''
Unit test to verify "autopilot" and its "vizualizer"

Created on May 3, 2010
@author: Bohdan Mushkevych
'''
import random
import unittest
import datagram, time
from autopilot import Autopilot
from test_route_transmission import TestRouteTransmission

from aviation_formulary import calculate_course_and_distance
from aviation_formulary import calculate_next_position

class TestAutopilot(unittest.TestCase):
		'''
		Unit tests covers full working cycle of autopilot:
		from setting the course to tracking the decisions of the autopilot in visualizer
				
		route is set between JFK and LAX (position in degrees): 
		point 1 (LAX): lat1 = 33 + 57/60 = 33.95;     lon1 = 118 + 24/60 = 118.4
		point 2 (JFK): lat2 = 40 + 38/60 = 40.633333; lon2 = 73 + 47/60 = 73.783333
		'''
		def setUp(self):
				self.route = TestRouteTransmission.set_route()
				
				# initial assignment of UAV position
				self.uav_longtitude = self.route.get_active().longtitude
				self.uav_latitude = self.route.get_active().latitude
				self.uav_altitude = self.route.get_active().altitude
				self.uav_direction = 0 # looking North

				# speed calculation variables
				self.last_timestamp = None

		def test_pilot(self):
				# start threads				
#				pilot = Autopilot()
#				pilot.start()
				
#				''' transmits route to the onboard autopilot and orders to take controls '''
				TestRouteTransmission.transmit_route(self.route)
				
				# command the autopilot to take control
				d = datagram.Datagram()
				d.set_destination(0xFFFF)
				d.set_sender(datagram.TEST_D_BITMASK)
				d.set_command(datagram.LWC_EXIT_MANUAL_MODE)
				d.pack_and_send()
				
#				while len(self.route) > 0:
#					self.calculate_next_point()
				

		def calculate_next_point(self):
				waypoint = self.route.get_active()

				# block of simple arithmetic calculation - altitude and airspeed
				uav_airspeed = 20 + random.randint(-10, +10) # 20 meters/sec = 72 km/hour
				distance_meters = self.calculate_distance(uav_airspeed)
				if self.uav_altitude > waypoint.altitude:
						self.uav_altitude = self.uav_altitude + random.randint(-5, +1) # going "more down than up"
				else:
						self.uav_altitude = self.uav_altitude + random.randint(-1, +5) # going "more up than down"

				# calculating direction		   
				print "lat: ", self.uav_latitude, "    long: ", self.uav_longtitude, "    waypoint.lat: ", waypoint.latitude, "    waypoint.long: ", waypoint.longtitude
				course, distance = calculate_course_and_distance(self.uav_latitude, self.uav_longtitude, waypoint.latitude, waypoint.longtitude)
				self.uav_direction = self.uav_direction - course + random.randint(-5, +5) # deviation of 5 degrees 

				# calculating lat/long of next point on glide
				if distance <= 0.999:
						## turn to next waypoint and reset uav position
						self.uav_longtitude = self.route.get_active().longtitude
						self.uav_latitude = self.route.get_active().latitude
						self.uav_altitude = self.route.get_active().altitude
						self.route.remove(waypoint)
				else:
						self.uav_latitude, self.uav_longtitude = calculate_next_position(self.uav_latitude, self.uav_longtitude, self.uav_direction, distance_meters)

				# now that we have calculated next point in glide - pack it and send it, pretending that it is from IMU
				d = datagram.Datagram()
				d.set_destination(0xFFFF)
				d.set_sender(datagram.TEST_D_BITMASK)
				d.set_command(datagram.LWC_DATA)
				d.add_float(datagram.STATE_D_LONGITUDE, self.uav_longtitude)
				d.add_float(datagram.STATE_D_LATITUDE, self.uav_latitude)
				d.add_float(datagram.STATE_D_ALTITUDE, self.uav_altitude)
				d.add_float(datagram.STATE_D_HEADING, self.uav_direction)
				d.add_float(datagram.STATE_D_VELOCITY_AIRSPEED, uav_airspeed)
				d.pack_and_send()


		def calculate_distance(self, uav_airspeed):
				''' Method is used to calculate the distance which plane has gone between the calls
				@param uav_airspeed: speed of the aircraft in meters/second
				@return distance the aircraft has done since the last call in meters'''

				if self.last_timestamp == None:
						self.last_timestamp = time.time() #number of seconds since Epoch
						distance_meters = 20 # we assume that UAV glides about 20 meters between next GPS update
				else:
						current_timestamp = time.time()
						distance_meters = uav_airspeed * (current_timestamp - self.last_timestamp)
						self.last_timestamp = time.time() #number of seconds since Epoch

				return distance_meters

if __name__ == "__main__":
		#import sys;sys.argv = ['', 'Test.testName']
		
		unittest.main()
