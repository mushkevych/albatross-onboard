'''
An abstraction for Servo module (controlling servo engines, which turns rudder, elevator and ailerons)

@date: Apr 24, 2010
@author: Bohdan Mushkevych
'''

import socket

HOSTNAME = '127.0.0.1'
FG_OUTPUT_PORT_NO = 16661
FG_INPUT_PORT_NO = 16662

class  Servo:

    def alter_course(self, angle_horizontal, angle_vertical, angle_roll):
        ''' 
        @param angle_horizontal: defines angle (in degrees) of turn for the rudder 
        @param angle_vertical: defines angle (in degrees) of turn for elevator 
        @param angle_roll: defines angle (in degrees) of turn for ailerons 
        
        @return tuple, presenting actual degrees the servo has executed
        (rudder, elevator, ailerons)'''
        
        # FlightGear stub
        # http://docs.python.org/howto/sockets.html
        # Set up a UDP socket, performing communication to FlightGear
        self.sender = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sender.connect((HOSTNAME, FG_INPUT_PORT_NO))
        message = "%f\t%f\t%f\n" % (angle_roll, angle_horizontal, angle_vertical)
        self.sender.send(message)
        self.sender.close()

#        print "*Servo* Horizontal: %f \t Vertical: %f \t Roll: %f" % (angle_horizontal, angle_vertical, angle_roll)
        return (angle_horizontal, angle_vertical, angle_roll)

