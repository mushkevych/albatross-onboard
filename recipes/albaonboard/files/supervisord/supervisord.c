/*
	Daemon to supervise 4 major onboard processes:
	- autopilot
	- communication daemon for antenna
	- communication daemon for IMU
	- gpsd client

	date: Aug 14, 2010
	author: Bohdan Mushkevych
 */

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "../protocol/albatross_std.h"
#include "../protocol/udp_multicast.h"
#include "../protocol/datagram.h"
#include "../protocol/protocol.h"

#define PROC_CONFIG_MAX_LINE_LENGTH 128
#define PROC_CONFIG_NAME "process_name=%s"
#define PROC_CONFIG_SHELL "process_shell=%s"
#define PROC_CONFIG_DIRECTORY "process_directory=%s"
#define PROC_CONFIG_PARAMETER_1 "process_parameter_1=%s"
#define PROC_CONFIG_PARAMETER_2 "process_parameter_2=%s"
#define PROC_NUMBER 3


typedef struct {
	// if pid == 0, process is considered either terminated or not started
	pid_t pid;
	char* directory;
	char* shell;
	char* command;
	char* parameter_1;
	char* parameter_2;
	int status;
} process_id;

/* INIT SECTION */
process_id pidAutopilot;
process_id pidCommd;
process_id pidCommdImu;
process_id pidGpsdClient;
int thread_is_running = TRUE;

//Datagram for sending
datagrizzle_t DgSend;
//Datagram for recieving
datagrizzle_t DgRecv;

void process_id_to_str(process_id *process)
{
	printf("process id: pid[%d] shell[%s] directory[%s] command[%s] param_1[%s] param_2[%s] \n", process->pid, process->shell, process->directory, process->command, process->parameter_1, process->parameter_2);
}

int init_from_file(char *filename)
{
	int i;
	FILE *inFile;
	char buff[PROC_CONFIG_MAX_LINE_LENGTH];

	printf("Configuring supervisord from file: %s \n", filename);

	//open the config file
	inFile = fopen(filename, "r");
	//Check the file was opened correctly
	if(inFile == NULL)
	{
		printf("Supervisord: Error Opening File \n");
		return FALSE;
	}

	for (i = 0; i < PROC_NUMBER; i++)
	{
		fgets(buff, PROC_CONFIG_MAX_LINE_LENGTH, inFile);
		char *name = malloc(64 * sizeof(char));
		sscanf(buff, PROC_CONFIG_NAME, name);

		fgets(buff, PROC_CONFIG_MAX_LINE_LENGTH, inFile);
		char *shell = malloc(64 * sizeof(char));
		sscanf(buff, PROC_CONFIG_SHELL, shell);

		fgets(buff, PROC_CONFIG_MAX_LINE_LENGTH, inFile);
		char *dir = malloc(96 * sizeof(char));
		sscanf(buff, PROC_CONFIG_DIRECTORY, dir);

		fgets(buff, PROC_CONFIG_MAX_LINE_LENGTH, inFile);
		char *par_1 = malloc(64 * sizeof(char));
		sscanf(buff, PROC_CONFIG_PARAMETER_1, par_1);

		fgets(buff, PROC_CONFIG_MAX_LINE_LENGTH, inFile);
		char *par_2 = malloc(64 * sizeof(char));
		sscanf(buff, PROC_CONFIG_PARAMETER_2, par_2);

		if (strcmp(name, "simple_autopilot_daemon.py") == 0)
		{
			pidAutopilot.command = name;
			pidAutopilot.shell = strcmp(shell, "") == 0 ? NULL : shell;
			pidAutopilot.directory = dir;
			pidAutopilot.parameter_1 = strcmp(par_1, "") == 0 ? NULL : par_1;
			pidAutopilot.parameter_2 = strcmp(par_2, "") == 0 ? NULL : par_2;

			process_id_to_str(&pidAutopilot);
		}
		else if (strcmp(name, "commd") == 0)
		{
			pidCommd.command = name;
			pidCommd.shell = strcmp(shell, "") == 0 ? NULL : shell;
			pidCommd.directory = dir;
			pidCommd.parameter_1 = strcmp(par_1, "") == 0 ? NULL : par_1;
			pidCommd.parameter_2 = strcmp(par_2, "") == 0 ? NULL : par_2;

			process_id_to_str(&pidCommd);
		}
		else if (strcmp(name, "commd_imu") == 0)
		{
			pidCommdImu.command = name;
			pidCommdImu.shell = strcmp(shell, "") == 0 ? NULL : shell;
			pidCommdImu.directory = dir;
			pidCommdImu.parameter_1 = strcmp(par_1, "") == 0 ? NULL : par_1;
			pidCommdImu.parameter_2 = strcmp(par_2, "") == 0 ? NULL : par_2;

			process_id_to_str(&pidCommdImu);
		}
		else if (strcmp(name, "alba_gpsd_client") == 0)
		{
			pidGpsdClient.command = name;
			pidGpsdClient.shell = strcmp(shell, "") == 0 ? NULL : shell;
			pidGpsdClient.directory = dir;
			pidGpsdClient.parameter_1 = strcmp(par_1, "") == 0 ? NULL : par_1;
			pidGpsdClient.parameter_2 = strcmp(par_2, "") == 0 ? NULL : par_2;

			process_id_to_str(&pidGpsdClient);
		} else {
			printf("name = '%s' unrecognised \n", name);
		}
	}

	return TRUE;
}

char* exec_error_to_string(int errnum)
{
	switch ( errnum ) {

	#ifdef EACCES
		case EACCES :
		{
			return "EACCES Permission denied";
		}
	#endif

	#ifdef EPERM
		case EPERM :
		{
			return "EPERM Not super-user";
		}
	#endif

	#ifdef E2BIG
		case E2BIG :
		{
			return "E2BIG Arg list too long";
		}
	#endif

	#ifdef ENOEXEC
		case ENOEXEC :
		{
			return "ENOEXEC Exec format error";
		}
	#endif

	#ifdef EFAULT
		case EFAULT :
		{
			return "EFAULT Bad address";
		}
	#endif

	#ifdef ENAMETOOLONG
		case ENAMETOOLONG :
		{
			return "ENAMETOOLONG path name is too long     ";
		}
	#endif

	#ifdef ENOENT
		case ENOENT :
		{
			return "ENOENT No such file or directory";
		}
	#endif

	#ifdef ENOMEM
		case ENOMEM :
		{
			return "ENOMEM Not enough core";
		}
	#endif

	#ifdef ENOTDIR
		case ENOTDIR :
		{
			return "ENOTDIR Not a directory";
		}
	#endif

	#ifdef ELOOP
		case ELOOP :
		{
			return "ELOOP Too many symbolic links";
		}
	#endif

	#ifdef ETXTBSY
		case ETXTBSY :
		{
			return "ETXTBSY Text file busy";
		}
	#endif

	#ifdef EIO
		case EIO :
		{
			return "EIO I/O error";
		}
	#endif

	#ifdef ENFILE
		case ENFILE :
		{
			return "ENFILE Too many open files in system";
		}
	#endif

	#ifdef EINVAL
		case EINVAL :
		{
			return "EINVAL Invalid argument";
		}
	#endif

	#ifdef EISDIR
		case EISDIR :
		{
			return "EISDIR Is a directory";
		}
	#endif

	#ifdef ELIBBAD
		case ELIBBAD :
		{
			return "ELIBBAD Accessing a corrupted shared lib";
		}
	#endif

		default :
		{
			if ( errnum ) return strerror(errnum);
		}
	}
}

int fork_process (process_id *process)
{
	if (process->pid != 0)
	{
		// process is running
		printf("WARNING: Child %s is already running \n", process->command);
		return 0;
	}

	int status = +1;
	pid_t pid = fork ();
	if (pid == 0)
	{
		/* This is the child process.  Execute the shell command. */
		chdir(process->directory);
		if (process->shell == NULL)
		{
			process->status = execl(process->command,
					process->command,
					process->parameter_1,
					process->parameter_2,
					NULL);
		}
		else
		{
			process->status = execl(process->shell,
					process->shell,
					process->command,
					process->parameter_1,
					process->parameter_2,
					NULL);
		}

		// Return not expected. Must be an execl error:
		printf("ERROR: execl failed: %s \n", exec_error_to_string(process->status));
		exit (EXIT_FAILURE);
	}
	else if (pid < 0)
	{
		/* The fork failed.  Report failure.  */
		printf("ERROR: fork failed \n");
		status = -1;
	}
	else
	{
		/* This is the parent process. Fork succeeded*/
		process->pid = pid;
		printf("Fork child process: %s with pid: %d \n", process->command, process->pid);
		status = 0;
	}

	// multi-threaded env. Flush is needed to output child's output streams
	fflush(NULL);
	return status;
}

int kill_process (process_id *process)
{
	if (process->pid == 0 || process->pid < 0)
	{
		// Avoid next use-cases:
		// If PID is zero, "kill" sends SIG to all processes in the current process's process group
		// If PID is < -1, send SIG to all processes in process group - PID
		return 0;
	}

	/* This is the parent process.  Kill the child process. */
	int status = kill(process->pid, SIGKILL);
	return status;
}

void datagram_callback(int cmd, void *user_data)
{
	if(cmd != -1)
	{
		if(cmd == LWC_START_ALL_DAEMONS)
		{
			printf("Starting All Daemons \n");
			fork_process(&pidCommdImu);
			fork_process(&pidCommd);
			fork_process(&pidGpsdClient);
			fork_process(&pidAutopilot);
			datagram_send_log_message("Starting All Daemons", SUPERVISOR_D_BITMASK, 0);
		}
		else if(cmd == LWC_STOP_ALL_DAEMONS)
		{
			printf("Stopping All Daemons \n");
			kill_process(&pidAutopilot);
			kill_process(&pidGpsdClient);
			kill_process(&pidCommd);
			kill_process(&pidCommdImu);
			datagram_send_log_message("Stopping All Daemons", SUPERVISOR_D_BITMASK, 0);
		}
		else if(cmd == LWC_RESTART_ALL_DAEMONS)
		{
			printf("Restarting All Daemons \n");
			kill_process(&pidAutopilot);
			kill_process(&pidGpsdClient);
			kill_process(&pidCommd);
			kill_process(&pidCommdImu);

			fork_process(&pidCommdImu);
			fork_process(&pidCommd);
			fork_process(&pidGpsdClient);
			fork_process(&pidAutopilot);
			datagram_send_log_message("Restarting All Daemons", SUPERVISOR_D_BITMASK, 0);
		}
		else if(cmd == LWC_SUPERVISORD_EXIT)
		{
			printf("Stopping All Daemons & Supervisord itself \n");
			datagram_send_log_message("Stopping All Daemons & Supervisord itself", SUPERVISOR_D_BITMASK, 0);
			kill_process(&pidAutopilot);
			kill_process(&pidGpsdClient);
			kill_process(&pidCommd);
			kill_process(&pidCommdImu);
			thread_is_running = FALSE;
		}
	}
	// multi-threaded env. Flush is needed to output child's output streams
	fflush(NULL);
}

void start_at_startup(void) 
{
	// Requesting all Daemons to Start
	datagram_clear(&DgSend);
	datagram_init(&DgSend, DATAGRAM_MAX_PAYLOAD_SIZE);
	datagram_set_sender(&DgSend, SUPERVISOR_D_BITMASK);
	datagram_set_destination(&DgSend, SUPERVISOR_D_BITMASK);
	datagram_set_command(&DgSend, LWC_START_ALL_DAEMONS);
	datagram_pack(&DgSend);
	datagram_send(&DgSend);
}

void query_child_process(process_id *process)
{
	if (process->pid == 0)
	{
		// process is either terminated or not started
		return;
	}

	/*
	 * if WNOHANG was specified and one or more child(ren) specified by pid exist,
	 * but have not yet changed state, then 0 is returned. On error, -1 is returned.
     */
	pid_t changed_pid;
	int childExitStatus;
	changed_pid = waitpid(process->pid, &childExitStatus, WNOHANG);

	if( changed_pid != 0 && WIFEXITED(childExitStatus) )
	{
		printf("Child %s terminated normally: Status= %d \n", process->command, WEXITSTATUS(childExitStatus));
		process->pid = 0;
		process->status = 0;
	}
	else if ( changed_pid != 0 && WIFSIGNALED(childExitStatus) )
	{
		printf("Child %s exited due to a signal: %d \n", process->command, WTERMSIG(childExitStatus));
		process->pid = 0;
		process->status = 0;
	}
}

int main(int argc, char *argv[])
{
	pthread_t rcv;
	struct timespec timer;
	init_from_file("processes.conf");

	//Initiation
	datagram_clear(&DgRecv);
	datagram_init(&DgRecv,DATAGRAM_MAX_PAYLOAD_SIZE);

	//Initialise the datagram callbacks
	datagram_set_receive_bitmask(&DgRecv, 0xFFFF);
	datagram_set_callback(&DgRecv, datagram_callback, NULL);

	//Now attach the thread and start it
	pthread_create(&rcv, NULL, &datagram_threaded_reciever, (void *)&DgRecv);
	datagram_send_log_message("Supervisord Started", SUPERVISOR_D_BITMASK, 0);

	//Start daemons
	sleep(1);
	start_at_startup();

	//Initialise the main loop
	timer.tv_sec = 0;
	timer.tv_nsec = 10000000;

	while(thread_is_running)
	{
		nanosleep(&timer, NULL);

		query_child_process(&pidAutopilot);
		query_child_process(&pidCommd);
		query_child_process(&pidCommdImu);
		query_child_process(&pidGpsdClient);
	}

	return 0;
}

